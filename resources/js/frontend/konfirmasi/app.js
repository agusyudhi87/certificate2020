
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '../../bootstrap';
import '../../plugins';
import Vue from 'vue';
import VeeValidate from 'vee-validate';
import VueSweetalert2 from 'vue-sweetalert2';
import Multiselect from 'vue-multiselect';
import VueMoment from 'vue-moment';
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';
import VueClockPicker from '@pencilpix/vue2-clock-picker';
import '@pencilpix/vue2-clock-picker/dist/vue2-clock-picker.min.css';
import Dayjs from 'vue-dayjs';

// import VueScrollTo  from 'vue-scrollto';

const VueUploadComponent = require('vue-upload-component')

window.Vue = Vue;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
Vue.use(BootstrapVue)
Vue.use(Dayjs)
Vue.use(VueSweetalert2)
Vue.use(VueMoment);
Vue.use(VeeValidate, {
    inject: true,
    fieldsBagName: "veeFields",
    errorBagName: "veeErrors"
});

// Vue.use(VueScrollTo, {
//      container: "body",
//      duration: 500,
//      easing: "ease",
//      offset: 0,
//      force: true,
//      cancelable: true,
//      onStart: false,
//      onDone: false,
//      onCancel: false,
//      x: false,
//      y: true
//  })
Vue.component('file-upload', VueUploadComponent)
Vue.component('multiselect', Multiselect)

Vue.component('form-konfirmasi', require('./components/FormKonfirmasi.vue').default);
Vue.component('notif-konfirmasi', require('./components/FormNotifKonfirmasi.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        choosedData:{},
        isShowForm:true,
        isShowNotification:false,
    },
    methods: {// receives a place object via the autocomplete component
                
        getData(value){
            if(value.action=='form'){
                this.isShowForm=true
                this.isShowNotification=false
            }else if(value.action=='notif'){
                this.isShowForm=false
                this.isShowNotification=true
            }
            this.choosedData = value.data
        }

    }
});