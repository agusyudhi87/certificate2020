@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
            <!-- <img src="{{config('app.url')}}/front/images/resources/logo-1-1.png" alt="{{config('app.name')}}"> -->
@endcomponent
@endslot

{{-- Body --}}
<div style="font-size: 16px!important;">
	{{ $slot }}
</div>

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent
