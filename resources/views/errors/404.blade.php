@extends('frontend.layouts.app')

@section('content')
            <section class="inner-banner">
                <div class="container">
                    <h2 class="inner-banner__title">Page Not Found - <a href="{{route('frontend.home')}}">Back Home</a></h2><!-- /.inner-banner__title -->
                    <ul class="thm-breadcrumb">
                        <li class="thm-breadcrumb__item active"><a class="thm-breadcrumb__link" href="{{route('frontend.home')}}">Back Home</a></li>
                        
                    </ul><!-- /.thm-breadcrumb -->
                </div><!-- /.container -->
            </section>
    </div><!--row-->
@endsection