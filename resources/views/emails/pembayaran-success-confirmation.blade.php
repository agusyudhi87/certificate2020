@component('mail::message')
	
Yth. {{$data->fullname}},
 <br>
Konfirmasi pendaftaran WITh PAPDI BALI 2020 telah kami terima
 <br>
LIVE WEBINAR akan dilakukan via ZOOM
 <br>
Link Webinar akan dibagikan beberapa waktu sebelum acara melalui email dan Whatsapp. Mohon pastikan anda log in dengan nama dan email yg terdaftar.
<br>
Demikian informasi ini disampaikan. Untuk informasi lebih lanjut dapat menghubungi: Dr. Hantono (0878 6752 7543)
 <br>

<b>
Terima kasih telah berpatisipasi,<br>
{{ config('app.name') }}
</b>
<br>
Ada pertanyaan? Silakan Hubungi :
<br>
+62 878-6752-7543 (dr Hantono)
@endcomponent