@component('mail::message')
	
Yth. {{$data->fullname}},
 <br>
Terima kasih atas pendaftaran WITh PAPDI BALI 2020,
<br>
Berikut adalah informasi rekening pembayaran 
<br>
<b>{{$data->payment}}</b>
<br>
ke rekening di bawah ini:
<br>
<b>BNI Cabang Renon
<br>
No. Rek: 0802334590
<br>
Atas Nama: PAPDI CABANG BALI
</b>

<br>
Setelah melakukan pembayaran, mohon melakukan konfirmasi ke link di bawah ini:
@component('mail::button', ['url' =>"https://withpapdibali.id/konfirmasi", 'color' => 'success'])
Konfirmasi Pembayaran
@endcomponent
<br>
<br>
Demikian informasi ini disampaikan. Untuk informasi lebih lanjut dapat menghubungi: Dr. Hantono (0878 6752 7543)
<b>
Terima kasih telah berpatisipasi,<br>
Panitia {{ config('app.name') }}
</b>
@endcomponent