@component('mail::message')
	
Yth. {{$data->name}},
 <br>
 <br>
Dengan hormat,
 <br>
Dengan ini, kami bermaksud menyampaikan terima kasih atas pendaftaran Bali Endocrine Update XVII 2020,
 <br>

Berikut kami lampirkan link zoom untuk webinar Bali Endocrine Update (BEU) XVII 2020 selama 3 hari 
 <br>

<a href="https://us02web.zoom.us/j/85311521711?pwd=VWpSc3JJMmpRQ2NGSGZoK2hmWmVDdz09">https://us02web.zoom.us/j/85311521711?pwd=VWpSc3JJMmpRQ2NGSGZoK2hmWmVDdz09 </a>
 <br/>
--------------------------
<br/>
Meeting ID: 853 1152 1711
<br/>
Passcode: BEU123
<br/>
--------------------------
<br/>
<br/>

Kapasitas zoom sebesar 1000 peserta. Jika mengalami kendala, prof/DR/dr dapat mengikuti webinar melalui live youtube di link di bawah ini
<br/>
<br/>

Senin, 9 November 2020
<br/>

<a href="https://youtu.be/SrmBMdkZDSU">https://youtu.be/SrmBMdkZDSU</a>
<br/>
<br/>

Selasa, 10 November 2020 
<br/>
<a href="https://youtu.be/DATjj99mm_o">https://youtu.be/DATjj99mm_o</a>
<br/>
<br/>

Rabu, 11 November 2020
<br/>
<a href="https://youtu.be/1ea7Q2X0M94">https://youtu.be/1ea7Q2X0M94</a>
<br/>
<br/>

Webinar akan berlangsung,
<br/>

Hari : Senin s/d Rabu
<br/>
Tanggal : 9 s/d 11 November 2020
<br/>
Waktu : 13:00 WITA
<br/>
<br/>

Untuk informasi lebih lanjut, silakan menghubungi dr. Sri Ariani 081805654445
<br/>

Atas perhatiannya, kami ucapkan terima kasih,
<br/>
<br/>

Salam, 
<br/>
Panitia {{ config('app.name') }}
</b>
<br>
@endcomponent