@component('mail::message')
	
Yth. {{$data->name}},
 <br><br>
 Dengan Hormat, <br>
 melalui email ini kami bermaksud menyampaikan terima kasih telah mengikuti Webinar ilmiah Optimizing Retinal Health & Function : Updates on Diagnostic and Therapeutic Approaches. Besar harapan kami dapat terus memberikan update-update terbaru dalam bidang ilmu Kesehatan Mata. 
 <br><br>
 Bersama email ini juga, kami sertakan sertifikat elektronik dalam attachment. Jika terdapat kegagalan atau kesalahan nama dapat menghubungi Eka Jala (ms.) di nomor: +62 812-4658-8255

<br>
<br>
Demikian informasi ini disampaikan. Atas perhatiannya, kami ucapkan terima kasih.
<br>
<br>

Salam sehat selalu,
<br>
<b>
Panitia Webinar {{ config('app.name') }}
</b>
<br>
@endcomponent