<!DOCTYPE html>
<html lang="en">

<head>
     <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <meta name="csrf-token" content="{{ csrf_token() }}">
     
    <title>WITh PAPDI BALI 2020 </title>
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('front')}}/images/favicon/fav.png">
    <link rel="shortcut icon" href="{{asset('front')}}/images/favicon/fav.png">
    <link rel="manifest" href="{{asset('front')}}/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('front')}}/images/favicon/fav.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="{{asset('front')}}/css/style.css">
    <link rel="stylesheet" href="{{asset('front')}}/css/responsive.css">
</head>

<body>
    <div class="preloader"><span></span></div><!-- /.preloader -->
    <div class="page-wrapper">
        <section class="topbar-one">
            <div class="container">
                <div class="topbar-one__left-text">ONLINE WEBINAR: OCTOBER 2020</div><!-- /.left-text -->
                <div class="topbar-one__right-content">
                    <a href="{{route('frontend.register')}}" class="topbar-one__btn">Register Now!</a>
                </div><!-- /.right-content -->
            </div><!-- /.container -->
        </section><!-- /.topbar-one -->
        <header class="site-header header-one">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="{{route('frontend.home')}}">
                            <img src="{{asset('front')}}/images/resources/logo-1-1.png" class="main-logo" alt="Awesome Image">
                            <img src="{{asset('front')}}/images/resources/logo-1-1.png" class="stick-logo" alt="Awesome Image">
                        </a>
                        <button class="menu-toggler" data-target=".main-navigation">
                            <span class="fa fa-bars"></span>
                        </button>
                    </div><!-- /.logo-box -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                        <ul class=" navigation-box">
                            <li><a href="{{route('frontend.home')}}">Home</a></li>                            
                            <li><a href="{{route('frontend.register')}}">Register</a></li>
                            <li><a href="{{route('frontend.register.konfirmasi')}}">Confirmation</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                    <div class="right-side-box" style="padding-left: 30px">
                        <a href="tel:1800-456-7890" class="header-one__cta">
                            <span class="header-one__cta-icon">
                                <i class="dentallox-icon-call-answer"></i>
                            </span>
                            <span class="header-one__cta-content">
                                <span class="header-one__cta-text">Call Us for question:</span>
                                <span class="header-one__cta-number">087867527543</span>
                            </span>
                        </a>
                    </div><!-- /.right-side-box -->
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.header-one -->
        
        <div id="app">
            @include('includes.partials.messages')
            @yield('content')
        </div><!-- container -->
        <footer class="site-footer">
            <div class="container ">
                <div class="footer-col">
                    <div class="footer-widget footer-widget__about">
                        <a class="footer-widget__logo" href="index.html"><img src="{{asset('front')}}/images/resources/logo-1-2.png" alt="Logo PAPDI BALI"></a>
                        
                    </div><!-- /.footer-widget -->
                    <div class="footer-widget">
                        <h3 class="footer-widget__title">
                            Useful Links
                        </h3>
                        <ul class="footer-widget__links-list">
                            <li class="footer-widget__links-list-item"> <a href="#">Home</a> </li><!-- /.footer-widget__links-list-item -->
                            <li class="footer-widget__links-list-item"> <a href="#">Register</a> </li><!-- /.footer-widget__links-list-item -->
                            <li class="footer-widget__links-list-item"> <a href="#">Konfirmasi Pembayaran</a> </li><!-- /.footer-widget__links-list-item -->
                            <li class="footer-widget__links-list-item"> <a href="#">Contact Us</a> </li><!-- /.footer-widget__links-list-item -->
                        </ul><!-- /.footer-widget__links-list -->
                    </div><!-- /.footer-widget -->
                    <div class="footer-widget">
                        <h3 class="footer-widget__title">
                            Sesi WITh PAPDI 2020
                        </h3>
                        <ul class="footer-widget__time-list">
                            <li class="footer-widget__time-list-item">Sesi 1: 10 Oktober 2020</li><!-- /.footer-widget__time-list-item -->
                            <li class="footer-widget__time-list-item">Sesi 2: 11 Oktober 2020 </li><!-- /.footer-widget__time-list-item -->
                            <li class="footer-widget__time-list-item">Sesi 3: 17 Oktober 2020 </li><!-- /.footer-widget__time-list-item -->
                            <li class="footer-widget__time-list-item">Sesi 4: 18 Oktober 2020 </li>
                        </ul><!-- /.footer-widget__time-list -->
                    </div><!-- /.footer-widget -->
                    <div class="footer-widget">
                        <h3 class="footer-widget__title">
                            Get In Touch
                        </h3>
                        <ul class="footer-widget__time-list no-margin">
                            <li class="footer-widget__time-list-item">Alamat Sekretariat PAPDI:</li><!-- /.footer-widget__time-list-item -->
                            <li class="footer-widget__time-list-item">Jl PB Sudirman no 12A Denpasar, Bali</li><!-- /.footer-widget__time-list-item -->
                            <li class="footer-widget__time-list-item">Phone : <a href="tel:+6287867527543">+62878 6752 7543</a></li><!-- /.footer-widget__time-list-item -->
                            <li class="footer-widget__time-list-item">Email : <a href="mailto:papdibali@yahoo.co.id">papdibali@yahoo.co.id</a></li><!-- /.footer-widget__time-list-item -->
                        </ul><!-- /.footer-widget__time-list -->
                        <div class="footer-widget__social">
                            <a href="https://instagram.com/papdi.bali"><i class="fa fa-instagram"></i></a>@papdi.bali
                        </div><!-- /.footer-widget__social -->
                    </div><!-- /.footer-widget -->
                </div><!-- /.footer-col -->
            </div><!-- /.container -->
            <div class="bottom-footer">
                <div class="container">
                    <hr class="bottom-footer__line">
                    <div class="bottom-footer__wrap">
                        <p class="bottom-footer__text">&copy; Copyright WITh PAPDI 2020 . All right reserved.</p><!-- /.bottom-footer__text -->
                        <p class="bottom-footer__text">Created by <a href="#">WITh PAPDI 2020</a></p><!-- /.bottom-footer__text -->
                    </div><!-- /.bottom-footer__wrap -->
                </div><!-- /.container -->
            </div><!-- /.bottom-footer -->
        </footer><!-- /.site-footer -->
    </div><!-- /.page-wrapper -->

    <!-- WhatsHelp.io widget -->
    <script type="text/javascript">
        (function () {
            var options = {// Facebook page ID
                whatsapp: "+6287867527543", // WhatsApp number 
                email: "papdibali@yahoo.co.id", 
                call: "+6287867527543", // Call phone number // URL of company logo (png, jpg, gif)
                company_logo_url: "https://withpapdibali.id/front/images/resources/logo-1-1.png", // URL of company logo (png, jpg, gif)
                greeting_message: "Hello, how may we help you? Just send us a message now to get assistance.", // Text of greeting message
                call_to_action: "Hi! Welcome to PAPDI BALI EVENT! Need Help? Just ask Our Team", // Call to action
                button_color: "#1E3968", // Color of button
                position: "right", // Position may be 'right' or 'left'
                order: "whatsapp,email,call" // Order of buttons
            };
            var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    </script>
    <!-- /WhatsHelp.io widget -->
    <!-- /.scroll-to-top -->
    <script src="{{asset('front')}}/js/jquery.js"></script>
    <script src="{{asset('front')}}/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('front')}}/js/bootstrap-select.min.js"></script>
    <script src="{{asset('front')}}/js/bootstrap-datepicker.min.js"></script>
    <script src="{{asset('front')}}/js/owl.carousel.min.js"></script>
    <script src="{{asset('front')}}/js/jquery.validate.min.js"></script>
    <script src="{{asset('front')}}/js/jquery.magnific-popup.min.js"></script>
    <script src="{{asset('front')}}/js/theme.js"></script>
    @stack('before-scripts')
       {!! script(mix('js/manifest.js')) !!}
       {!! script(mix('js/vendor.js')) !!}
    @stack('after-scripts')
    @yield('pagespecificscripts')
</body>

</html>