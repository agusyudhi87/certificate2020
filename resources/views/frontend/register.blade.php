@extends('frontend.layouts.app')

@section('content')
            <section class="inner-banner">
                <div class="container">
                    <h2 class="inner-banner__title">REGISTRASI - WITh PAPDI 2020</h2>
                    <ul class="thm-breadcrumb">
                        <li class="thm-breadcrumb__item"><a class="thm-breadcrumb__link" href="{{route('frontend.home')}}">Home</a></li>
                        <li class="thm-breadcrumb__item current"><a class="thm-breadcrumb__link" href="{{route('frontend.register')}}">Registrasi</a></li>
                    </ul>
                </div>
            </section>
      <div class="container" id="notifelement">
        <section class="blog-two">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="service-sidebar">
                            <div class="service-sidebar__single">
                                <div class="service-sidebar__cta">
                                    <h3 class="service-sidebar__cta-title">WITh PAPDI 2020 Teaser</h3>
                                    <a href="https://youtu.be/XvdXX7IsTa4" class="thm-btn  hvr-pulse video-popup">See Video</a>
                                </div>

                            </div>
                            <div class="service-sidebar__single">
                                <div class="service-sidebar__contact">
                                   <!--  <p class="service-sidebar__contact-text"><i class="fa fa-phone"></i><a href="teL:1800-456-7890" class="service-sidebar__contact-highlight">1800 456 7890</a></p> -->
                                    <p class="service-sidebar__contact-text"><i class="fa fa-paper-plane"></i><a href="mailto:papdibali@yahoo.co.id">papdibali@yahoo.co.id</a></p>
                                    <p class="service-sidebar__contact-text"><i class="fa fa-phone"></i><a href="tel:087867527543">087867527543</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="patient-content">
                            <p class="patient-tag-line">PAPDI BALI 2020 Registration</p>
                            <br>
                            <div v-if="isShowForm">
                                <p class="patient-text">We want to thank you for joining WITh PAPDI BALI 2020.</p>
                                <br>
                                <form-registrasi @data-registrasi="getData"></form-registrasi>
                                
                            </div>
                            <div v-if="isShowNotification">
                                <notif-registrasi :dataprop="choosedData"></notif-registrasi>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    </div>
@endsection
@section('pagespecificscripts')
    {!! script(mix('js/registrasi.js')) !!}
@stop