@extends('frontend.layouts.app')

@section('content')
            <section class="inner-banner">
                <div class="container">
                    <h2 class="inner-banner__title">WITh PAPDI 2020</h2><!-- /.inner-banner__title -->
                    <ul class="thm-breadcrumb">
                        <li class="thm-breadcrumb__item current"><a class="thm-breadcrumb__link" href="{{route('frontend.home')}}">Home</a></li>
                    </ul><!-- /.thm-breadcrumb -->
                </div><!-- /.container -->
            </section><!-- /.inner-banner -->
            <div class="container">
        <section class="blog-two">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="blog-two__single">
                            <div class="blog-two__image">
                                <img src="{{asset('front')}}/images/blog/papdi.jpg" alt="WITh PAPDI Bali">
                               <!--  <div class="blog-two__date">
                                    <span class="blog-two__date-number">ONLINE WEBINAR</span>
                                </div> --><!-- /.blog-two__date -->
                            </div><!-- /.blog-two__image -->
                            <ul class="blog-two__meta">
                                <li class="blog-two__meta-item">
                                    <a href="#" class="blog-two__meta-link"><i class="fa fa-user"></i>By :  WITh PAPDI BALI</a>                                    
                                </li>
                                <li class="blog-two__meta-item">
                                    <a href="#" class="blog-two__meta-link"><i class="fa fa-tag"></i>WEBINAR</a>                                    
                                </li>
                            </ul><!-- /.blog-two__meta -->

                            <h2 class="blog-two__title">WITh PAPDI BALI 2020 </h2><!-- /.blog-two__title -->
                            <p class="blog-two__text">

                                
<p>
    <strong>TUJUAN</strong>
</p>
<ol>
    <li>Umum
        <p>Meningkatkan pengetahuan dan profesionalisme tenaga medis dalam agar dapat memberikan pelayanan yang paripurna kepada masyarakat ditengah pandemi saat ini.</p>
    </li>
    <li>Khusus
        <ul>
            <li>Memberikan informasi dan ilmu pengetahuan terkini di bidang ilmu penyakit dalam</li>
            <li>Meningkatkan keterampilan klinis medis di bidang ilmu penyakit dalam</li>
        </ul>

    </li>
</ol>
<p>
    <strong>TEMA</strong>
</p>
<p>
    <em>Internal Medicine Updates in Response to COVID-19 Pandemic&rdquo;</em>
</p>
<p>
    <strong>TEMPAT DAN WAKTU</strong>
</p>
<p>Tempat : Online (Webinar)</p>
<p></p>
<p>Waktu : Oktober 2020
    <ul>
    <li>Seri 1 : Tanggal 10 Oktober 2020</li>
    <li>Seri 2 : Tanggal 11 Oktober 2020</li>
    <li>Seri 3 : Tanggal 17 Oktober 2020</li>
    <li>Seri 4 : Tanggal 18 Oktober 2020</li>
    </ul>

</p>

<p>
    <strong>SEKRETARIAT PANITIA</strong>
</p>
<p>Sekretariat PAPDI Bali, Jln. PB Sudirman 12A Denpasar, Bali</p>
<p></p>
<p></p>
<p>
    <strong>PESERTA</strong>
</p>
<ul>
    <li>Dokter Spesialis Penyakit Dalam</li>
    <li>Dokter Spesialis lainnya</li>
    <li>Dokter umum/puskesmas, PPDS Internship</li>
    <li>Mahasiswa Kedokteran</li>
</ul>
<p>
    <strong>BIAYA REGISTRASI</strong>
</p>
<ul>
    <li>Dokter  Rp. 100.000,-</li>
    <li>Spesialis Rp. 200.000,-</li>
</ul>

<p>
    <strong>TOPIK WEBINAR DI BIDANG:</strong>
</p>
<ul>
    <li>Endokrinologi dan Metabolisme</li>
    <li>Hematologi Onkologi Medik</li>
    <li>Gastroentero-Hepatologi</li>
    <li>Alergi-Imunologi Klinik</li>
    <li>Ginjal dan Hipertensi</li>
    <li>Tropik dan Infeksi</li>
    <li>Reumatologi</li>
    <li>Pulmonologi</li>
    <li>Geriatri</li>
</ul>





                            </p><!-- /.blog-two__text -->
                            <br>
                            <a href="{{route('frontend.register')}}" class="thm-btn" style="color:white">Registrasi Now!</a>

                        </div><!-- /.blog-two__single -->

                    </div><!-- /.col-lg-9 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.blog-two -->
    </div><!--row-->
    </div><!--row-->
@endsection
