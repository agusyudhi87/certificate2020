@extends('frontend.layouts.app')

@section('content')
            <section class="inner-banner">
                <div class="container">
                    <h2 class="inner-banner__title">KONFIRMASI - WITh PAPDI 2020</h2><!-- /.inner-banner__title -->
                    <ul class="thm-breadcrumb">
                        <li class="thm-breadcrumb__item"><a class="thm-breadcrumb__link" href="{{route('frontend.home')}}">Home</a></li>
                        <li class="thm-breadcrumb__item current"><a class="thm-breadcrumb__link" href="{{route('frontend.register.konfirmasi')}}">Konfirmasi Pembayaran</a></li>
                        
                    </ul><!-- /.thm-breadcrumb -->
                </div><!-- /.container -->
            </section><!-- /.inner-banner -->
    <div class="container" id="notifelement">
        <section class="blog-one">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="patient-content">
                            <p class="patient-tag-line">PAPDI BALI 2020 Konfirmasi Transfer</p><!-- /.patient-tag-line -->
                            <br>
                            <p class="patient-text">We want to thank you for joining WITh PAPDI BALI 2020.</p><!-- /.patient-text -->
                            <br>
                            <div v-if="isShowForm">
                                <form-konfirmasi @data-konfirmasi="getData"></form-konfirmasi>
                            </div>
                            <div v-if="isShowNotification">
                                <notif-konfirmasi></notif-konfirmasi>
                            </div>
                        </div><!-- /.patient-content -->
                    </div><!-- /.col-lg-9 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.blog-two -->
    </div><!--row-->
    </div><!--row-->
@endsection
@section('pagespecificscripts')
    {!! script(mix('js/konfirmasi.js')) !!}
@stop