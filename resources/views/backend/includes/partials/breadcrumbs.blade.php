@if($breadcrumbs)
    <h3 class="kt-subheader__title">{{ $breadcrumbs[count($breadcrumbs)-1]->title }}</h3>
    <span class="kt-subheader__separator kt-hidden"></span>
    <div class="kt-subheader__breadcrumbs">
        <a href="{{ $breadcrumbs[0]->url }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
        <span class="kt-subheader__breadcrumbs-separator"></span>

            @foreach($breadcrumbs as $breadcrumb)
                 <a href="{{ $breadcrumb->url }}" class="kt-subheader__breadcrumbs-link">{{ $breadcrumb->title }} </a>
                @if($breadcrumb->url && !$loop->last)
                    <span class="kt-subheader__breadcrumbs-separator"></span>
                @endif
            @endforeach

            @yield('breadcrumb-links')
    </div>
@endif
