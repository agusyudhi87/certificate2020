@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <div class="row">
        <div class="col-xl-12 col-lg-12 order-lg-1 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                           {{ __('labels.backend.access.users.active') }}
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">

                        <div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="" data-placement="left" data-original-title="Quick actions">
                            <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                        <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                        <path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000"></path>
                                    </g>
                                </svg>

                                <!--<i class="flaticon2-plus"></i>-->
                            </a>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">

                                <!--begin::Nav-->
                                <ul class="kt-nav">
                                    <li class="kt-nav__head">
                                        Table Options:
                                        <i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Click to learn more..."></i>
                                    </li>
                                    <li class="kt-nav__separator"></li>
                                    <li class="kt-nav__item">
                                        <a href="{{ route('admin.auth.user.index') }}" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-user"></i>
                                            <span class="kt-nav__link-text">@lang('menus.backend.access.users.all')</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="{{ route('admin.auth.user.create') }}" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-add"></i>
                                            <span class="kt-nav__link-text">@lang('menus.backend.access.users.create')</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="{{ route('admin.auth.user.deactivated') }}" class="kt-nav__link">
                                            <i class="kt-nav__link-icon 
fa fa-window-close"></i>
                                            <span class="kt-nav__link-text">@lang('menus.backend.access.users.deactivated')</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="{{ route('admin.auth.user.deleted') }}" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-rubbish-bin-delete-button"></i>
                                            <span class="kt-nav__link-text">@lang('menus.backend.access.users.deleted')</span>

                                        </a>
                                    </li>
                                </ul>

                                <!--end::Nav-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body kt-portlet__body--fit">

                    <!--begin: Datatable -->
                    <div class="card">
                        <div class="card-body">

                            <div class="row mt-4">
                                <div class="col">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>@lang('labels.backend.access.users.table.last_name')</th>
                                                    <th>@lang('labels.backend.access.users.table.first_name')</th>
                                                    <th>@lang('labels.backend.access.users.table.email')</th>
                                                    <th>@lang('labels.backend.access.users.table.confirmed')</th>
                                                    <th>@lang('labels.backend.access.users.table.roles')</th>
                                                    <th>@lang('labels.backend.access.users.table.other_permissions')</th>
                                                    <th>@lang('labels.backend.access.users.table.social')</th>
                                                    <th>@lang('labels.backend.access.users.table.last_updated')</th>
                                                    <th>@lang('labels.general.actions')</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($users as $user)
                                                <tr>
                                                    <td>{{ $user->last_name }}</td>
                                                    <td>{{ $user->first_name }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>@include('backend.auth.user.includes.confirm', ['user' => $user])</td>
                                                    <td>{{ $user->roles_label }}</td>
                                                    <td>{{ $user->permissions_label }}</td>
                                                    <td>@include('backend.auth.user.includes.social-buttons', ['user' => $user])</td>
                                                    <td>{{ $user->updated_at->diffForHumans() }}</td>
                                                    <td class="btn-td">@include('backend.auth.user.includes.actions', ['user' => $user])</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!--col-->
                            </div>
                            <!--row-->
                            <div class="row">
                                <div class="col-5">
                                    <div class="float-left">
                                        {!! $users->total() !!} {{ trans_choice('labels.backend.access.users.table.total', $users->total()) }}
                                    </div>
                                </div>
                                <!--col-->
                                <div class="col-sm-2 float-right">
                                    @include('backend.auth.user.includes.header-buttons')
                                </div>
                                <!--col-->
                                <div class="col-5">
                                    <div class="float-right">
                                        {!! $users->render() !!}
                                    </div>
                                </div>
                                <!--col-->
                            </div>
                            <!--row-->
                        </div>
                        <!--card-body-->
                    </div>
                    <!--card-->

                    <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection