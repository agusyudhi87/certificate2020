<?php

use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\RegisterController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\Auth\LoginController;
/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
// Route::redirect('/', '/login', 301);
//Route::get('/', [LoginController::class, 'showLoginForm'])->name('index');
Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');

Route::get('register', [RegisterController::class, 'register'])->name('register');
Route::post('register', [RegisterController::class, 'registerproses'])->name('register.proses');

Route::get('konfirmasi', [RegisterController::class, 'konfirmasi'])->name('register.konfirmasi');
Route::post('konfirmasi', [RegisterController::class, 'konfirmasiproses'])->name('register.konfirmasi');
Route::post('upload/bukti', [RegisterController::class, 'uploadBukti'])->name('upload.bukti');
/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        // User Dashboard Specific
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

        // User Account Specific
        Route::get('account', [AccountController::class, 'index'])->name('account');

        // User Profile Specific
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
    });
});
