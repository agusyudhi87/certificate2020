<?php


use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\RegisterController;
use App\Http\Controllers\Backend\SertifikasiController;
use Arcanedev\LogViewer\Http\Controllers\LogViewerController;




// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/register', 301);
// Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');




//Register
Route::get('register', [RegisterController::class, 'register'])->name('register');
Route::get('register/payment/{id}', [RegisterController::class, 'registerpayment'])->name('register.payment');
Route::get('register/list', [RegisterController::class, 'listregister'])->name('register.list');
Route::post('register/add', [RegisterController::class, 'saveregister'])->name('register.save');
Route::post('register/edit/{id}', [RegisterController::class, 'updateregister'])->name('register.update');
Route::post('register/delete/{id}', [RegisterController::class, 'deleteregister'])->name('register.delete');

//Register
Route::get('konfirmasi', [RegisterController::class, 'konfirmasi'])->name('konfirmasi');
Route::get('konfirmasi/list', [RegisterController::class, 'listkonfirmasi'])->name('konfirmasi.list');
Route::post('konfirmasi/add', [RegisterController::class, 'savekonfirmasi'])->name('konfirmasi.save');
Route::post('konfirmasi/edit/{id}', [RegisterController::class, 'updatekonfirmasi'])->name('konfirmasi.update');
Route::post('konfirmasi/delete/{id}', [RegisterController::class, 'deletekonfirmasi'])->name('konfirmasi.delete');


//Register
Route::get('sertifikasi', [SertifikasiController::class, 'sertifikasi'])->name('sertifikasi');
Route::get('sertifikasi/list', [SertifikasiController::class, 'listsertifikasi'])->name('sertifikasi.list');
Route::post('sertifikasi/add', [SertifikasiController::class, 'savesertifikasi'])->name('sertifikasi.save');
Route::post('sertifikasi/generate', [SertifikasiController::class, 'generatesertifikasi'])->name('sertifikasi.generate');
// Route::post('konfirmasi/delete/{id}', [RegisterController::class, 'deletekonfirmasi'])->name('konfirmasi.delete');
Route::get('sendmail', [SertifikasiController::class, 'sendEmail'])->name('sendmail');

Route::post('upload/bukti', [SertifikasiController::class, 'uploadFile'])->name('upload.file');



Route::get('log-viewer', [LogViewerController::class, 'index'])->name('logviewer');
Route::get('log-viewer/list', [LogViewerController::class, 'listLogs'])->name('logviewer.list');
Route::get('log-viewer/show/{date}', [LogViewerController::class, 'show'])->name('show');


Route::get('/perintah/{name}', function ($name) {
    Artisan::call('make:repository', ['name' => 'Backend\\' . $name . 'Repository']);
    Artisan::call('make:controller', ['name' => 'Backend\\' . $name . 'Controller']);
    Artisan::call('make:request', ['name' => 'Backend\\' . $name . 'Request']);
});
