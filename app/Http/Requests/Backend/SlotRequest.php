<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class SlotRequest extends FormRequest
{
    protected function prepareForValidation()
    {
        $this->merge([
            'courseplan' => json_decode($this->courseplan),
            'room' => json_decode($this->room),
            'day_index' => json_decode($this->day_index),
        ]);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             
        ];
    }




}
