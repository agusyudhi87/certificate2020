<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class CourseAttendanceRequest extends FormRequest
{
    protected function prepareForValidation()
    {
        $this->merge([
            'courseplanitem' => json_decode($this->courseplanitem),
            'slot' => json_decode($this->slot),
            'lecture' => json_decode($this->lecture)
        ]);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
