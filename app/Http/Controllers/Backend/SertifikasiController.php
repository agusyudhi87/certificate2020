<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Papdi\Sertifikasi;
use App\Models\Papdi\SertifikasiDetail;
use App\Http\Requests\Backend\SertifikasiRequest;
use Illuminate\Http\Request;
use App\Exports\SertifikasiExport;
use PhpOffice\PhpSpreadsheet\IOFactory as IOExcel;
use PhpOffice\PhpWord\IOFactory as IOWord;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\Shared\ZipArchive;
use App\Events\Backend\BlastSertifikat;
use DPDF;
// use Unoconv;


class SertifikasiController extends Controller
{

    public function sertifikasi()
    {
        return view('backend.papdi.sertifikasi');
    }

    public function listsertifikasi(){
       return Sertifikasi::get();         
    }

    public function savesertifikasi(SertifikasiRequest $request)
    {
       // return $this->repository->create($request->all());
    }

    public function readExcel(String $path){
    	$objPHPExcel = IOExcel::load($path);
	    $worksheet = $objPHPExcel->getActiveSheet();
	    $rows = [];
		foreach ($worksheet->getRowIterator() AS $row) {
		    $cellIterator = $row->getCellIterator();
		    $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
		    $cells = [];
		    foreach ($cellIterator as $cell) {
		        $cells[] = $cell->getValue();
		    }
		    $rows[] = $cells;
		}

		return $rows;
    }
    public function generatesertifikasi(SertifikasiRequest $request){
        $data = $request->all();
        $path = public_path($data['filename']);
        $templatePath = public_path('template/sertif.docx');
		$excels = $this->readExcel($path);

		$wordExt = '.docx';
		$pdfExt = '.pdf';
		$pdfFolder = 'tempPDF/';
		$wordFolder = 'tempDoc/';

		// dd($excels);

		foreach($excels as $excel){
			//generate Doc
			$templateProcessor = new TemplateProcessor($templatePath);
			$templateProcessor->setValues([
				'nama' => $excel[1]
			]);
			$filename = $data['event'].' - '.$excel[1];
			header("Content-Disposition: attachment;");
			$templateProcessor->saveAs($wordFolder.$filename.$wordExt);


			//Input Database
			$dataInsert = [
				'name' => $excel[1],
				'email' => $excel[3],
				'tgl_event' => $data['tanggal'],
				'nama_event' => $data['event'],
				'file' => $pdfFolder.$filename.$pdfExt,
			];

			$saveDatabase = SertifikasiDetail::insert($dataInsert);

			// $unoconv = Unoconv\Unoconv::create(array(
			// 	'timeout'          => 42,
			// 	'unoconv.binaries' => '/opt/local/unoconv/bin/unoconv',
			// ), $logger);

		}

		// $phpWord = IOWord::load($filename); 


		// DPDF::loadFile(public_path().'/'.$filename)->save(public_path().'/compilePDF/baru.pdf');


		// //DOMPDF
		// $domPdfPath = base_path( 'vendor/dompdf/dompdf');
		// Settings::setPdfRendererPath($domPdfPath);
		// Settings::setPdfRendererName('DomPDF');

		// $phpWord = IOWord::load($filename, "Word2007"); 

		// $xmlWriter = IOWord::createWriter($phpWord , 'PDF');
		// $xmlWriter->save('compilePDF/'.$pdfname);  


		//TCPDF
		// $domPdfPath = base_path( 'vendor/tecnickcom/tcpdf');
		// Settings::setPdfRendererPath($domPdfPath);
		// Settings::setPdfRendererName('TCPDF');

		// $phpWord = IOWord::load('temp.docx'); 
		// $xmlWriter = IOWord::createWriter($phpWord , 'PDF');
		// $xmlWriter->save('compilePDF/resultPDFDODE.pdf');  
		// $this->toPDF();
		// dd($phpWzord);

   //      $rows = [];
   //      foreach ($excels as $row) {
			// $rows[]=[
			//     	'nama'=>$row[1],	
			//     	'email'=>$row[2],	
			//     	'tgl_event'=>$row[3],	
			//     	'nama_event'=>$row[4],	
			//     ];
        	
   //      }
   //      dd($rows);
	}
	

    // public function sendEmail(){
	// 	$jmlLimit = 5;

	// 	$allSertifikat = SertifikasiDetail::whereStatus(0)
	// 	   ->limit($jmlLimit)
	// 	   ->get();
		
	// 	// dd($allSertifikat);

   	//     foreach ($allSertifikat as $sertifikat) {
   	//     	// if($sertifikat->status===0){
   	//     		// dd(public_path($sertifikat->file));
	// 	        $sertifikat->status = 1;
	// 	        $sertifikat->save();
	// 	    	event(new BlastSertifikat($sertifikat));
   	//     	// }
   	    	
   	//     }
    //     return response()->json(['message' => 'Sukses']);

    // }

    public function toPDF(){
    	$templateFile = public_path('template/sertif.docx');
		$generatedFile = public_path('template/generated.docx');
		$targetFile    = "MergeResult.docx";
		 
		// copy template to target
		copy($templateFile, $targetFile);
		 
		// open target
		$targetZip = new ZipArchive();
		$targetZip->open($targetFile);
		$targetDocument = $targetZip->getFromName('word/document.xml');
		$targetDom      = new \DOMDocument();
		$targetDom->loadXML($targetDocument);
		$targetXPath = new \DOMXPath($targetDom);
		$targetXPath->registerNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
		 
		// open source
		$sourceZip = new \ZipArchive();
		$sourceZip->open($generatedFile);
		$sourceDocument = $sourceZip->getFromName('word/document.xml');
		$sourceDom      = new \DOMDocument();
		$sourceDom->loadXML($sourceDocument);
		$sourceXPath = new \DOMXPath($sourceDom);
		$sourceXPath->registerNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
		 
		/** @var DOMNode $replacementMarkerNode node containing the replacement marker $CONTENT$ */
		$replacementMarkerNode = $targetXPath->query('//w:p[contains(translate(normalize-space(), " ", ""),"$CONTENT$")]')[0];
		 
		// insert source nodes before the replacement marker
		$sourceNodes = $sourceXPath->query('//w:document/w:body/*[not(self::w:sectPr)]');
		 
		foreach ($sourceNodes as $sourceNode) {
		    $imported = $replacementMarkerNode->ownerDocument->importNode($sourceNode, true);
		    $inserted = $replacementMarkerNode->parentNode->insertBefore($imported, $replacementMarkerNode);
		}
		 
		// remove $replacementMarkerNode from the target DOM
		$replacementMarkerNode->parentNode->removeChild($replacementMarkerNode);
		 
		// save target
		$targetZip->addFromString('word/document.xml', $targetDom->saveXML());
		$targetZip->close();
    }

    public function uploadFile(SertifikasiRequest $request){
        $file = $request->file('file_upload');
        $tujuan_upload = 'file_upload';
        $file->move($tujuan_upload,$file->getClientOriginalName());
        $filename = $tujuan_upload."/".$file->getClientOriginalName();
        return response()->json(['filename' => $filename]);
    }




}
