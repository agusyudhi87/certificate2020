<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Papdi\Peserta;
use App\Models\Papdi\Konfirmasi;
use App\Mail\RegisterMail;
use App\Mail\PembayaranMail;
use App\Http\Requests\Frontend\RegisterRequest;
use App\Events\Backend\AdminKonfirmasi;


class RegisterController extends Controller
{

    public function register()
    {
        return view('backend.papdi.register');
    }

    public function listregister(){
        return Peserta::get();
    }    

    public function registerpayment($id){
        $peserta = Peserta::find($id);
        $peserta->payment = 1;
        $peserta->save();
        event(new AdminKonfirmasi($peserta));
        return $peserta;
    }    

    public function konfirmasi()
    {
        return view('backend.papdi.konfirmasi');
    }

    public function listkonfirmasi(){
       return Konfirmasi::get();         
    }

    public function save(RoomRequest $request)
    {
       return $this->repository->create($request->all());
    }

    public function update(RoomRequest $request, String $id)
    {
        // dd($id);
       return $this->repository->update($request->all(),$id);
    }

    public function delete(String $id)
    {
       return $this->repository->delete($id);
    }    






}
