<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Requests\Frontend\RegisterRequest;
use App\Http\Controllers\Controller;
use App\Models\Papdi\Peserta;
use App\Models\Papdi\Konfirmasi;
use App\Mail\RegisterMail;
use App\Mail\PembayaranMail;
use App\Events\Frontend\PesertaRegistrasi;
use App\Events\Frontend\PesertaKonfirmasi;
use Illuminate\Support\Facades\Mail;


/**
 * Class HomeController.
 */
class RegisterController extends Controller
{


    public function register()
    {
        return view('frontend.register');
    }    

    public function registerproses(RegisterRequest $request)
    {
	    $data = $request->all();
        $payment = ($data['profesi']=="dokter umum")?"Rp 100.000":"Rp 200.000";
        $peserta = Peserta::updateOrCreate($data);
        $peserta['payment'] = $payment;

        event(new PesertaRegistrasi($peserta));
        return $peserta;
    }    

    public function konfirmasi(RegisterRequest $request)
    {
	  	return view('frontend.konfirmasi');
    }

    public function konfirmasiproses(RegisterRequest $request)
    {
	    $data = $request->all();
        $konfirmasi = Konfirmasi::updateOrCreate($data);
        

        event(new PesertaKonfirmasi($konfirmasi));
        return $konfirmasi;
    } 

    public function uploadBukti(RegisterRequest $request){
        $file = $request->file('bukti_transfer');
        $tujuan_upload = 'bukti_transfer';
        $file->move($tujuan_upload,$file->getClientOriginalName());
    }
}
