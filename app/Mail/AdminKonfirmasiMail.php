<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminKonfirmasiMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->subject('PEMBAYARAN DITERIMA - PAPDI BALI 2020')
                ->attach(public_path($data->file), [
                         'as' => 'sample.pdf',   
                         'mime' => 'application/pdf'
                    ])
                ->markdown('emails.pembayaran-success-confirmation');
    }
}
