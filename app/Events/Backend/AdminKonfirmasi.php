<?php

namespace App\Events\Backend;

use App\Models\Papdi\Peserta;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserLoggedOut.
 */
class AdminKonfirmasi
{
    use Dispatchable, InteractsWithSockets,SerializesModels;

    /**
     * @var
     */
    public $peserta;


    /**
     * @param $peserta
     */
    public function __construct(Peserta $peserta)
    {
        $this->peserta =  $peserta;
    }
}
