<?php

namespace App\Events\Backend;

use App\Models\Papdi\SertifikasiDetail;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserLoggedOut.
 */
class BlastEventBEU
{
    use Dispatchable, InteractsWithSockets,SerializesModels;

    /**
     * @var
     */
    public $sertifikasi;


    /**
     * @param $peserta
     */
    public function __construct(SertifikasiDetail $sertifikasi)
    {
        $this->sertifikasi =  $sertifikasi;
    }
}
