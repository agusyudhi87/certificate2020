<?php

namespace App\Events\Backend;

use App\Models\Papdi\Whatsapp;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserLoggedOut.
 */
class BlastWhatsapp
{
    use Dispatchable, InteractsWithSockets,SerializesModels;

    /**
     * @var
     */
    public $whatsapp;


    /**
     * @param $peserta
     */
    public function __construct(Whatsapp $whatsapp)
    {
        $this->whatsapp =  $whatsapp;
    }
}
