<?php

namespace App\Events\Frontend;

use App\Models\Papdi\Peserta;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserLoggedOut.
 */
class PesertaRegistrasi
{
    use Dispatchable, InteractsWithSockets,SerializesModels;

    /**
     * @var
     */
    public $peserta;


    /**
     * @param $peserta
     */
    public function __construct(Peserta $peserta)
    {
        $this->peserta =  $peserta;
    }
}
