<?php

namespace App\Events\Frontend;

use App\Models\Papdi\Konfirmasi;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserLoggedOut.
 */
class PesertaKonfirmasi
{
    use SerializesModels;

    /**
     * @var
     */
    public $konfirmasi;

    /**
     * @param $peserta
     */
    public function __construct(Konfirmasi $konfirmasi)
    {
        $this->konfirmasi = $konfirmasi;
    }
}
