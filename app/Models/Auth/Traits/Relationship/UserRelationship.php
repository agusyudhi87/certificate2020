<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\PasswordHistory;
use App\Models\Auth\SocialAccount;
use App\Models\Sisfo\Employee;
use App\Models\Sisfo\Student;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class);
    }

    /**
     * UserRelationship has one Employee.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function employee()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = userRelationship_id, localKey = id)
        return $this->hasOne(Employee::class,'user_id');
    }

    public function student()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = userRelationship_id, localKey = id)
        return $this->hasOne(Student::class,'user_id');
    }
}
