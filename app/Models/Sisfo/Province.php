<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Regency;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provincies';
    protected $fillable = [
        'id', 'code', 'name'
    ];

    public function regencies()
    {
        return $this->hasMany(Regency::class);
    }
}
