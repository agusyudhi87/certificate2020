<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;

class StudentFinalProjects extends Model
{
    protected $primaryKey = null;
    public $incrementing = false;

    protected $table = 'student_final_projects';
    protected $fillable = [
        'student_id', 'project', 'pre_evaluation', 'final_evaluation'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
