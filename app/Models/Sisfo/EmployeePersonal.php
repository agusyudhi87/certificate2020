<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;

class EmployeePersonal extends Model
{
    protected $primaryKey = null;
    public $incrementing = false;

   protected $fillable = [
       'employee_id', 'birthdate', 'birthplace', 'phone', 'email', 'tax_number'
   ];

   public function employee(){
       return $this->belongsTo(Employee::class);
   }

}
