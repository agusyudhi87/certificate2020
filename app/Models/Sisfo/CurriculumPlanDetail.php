<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\CurriculumPlan;
use Illuminate\Database\Eloquent\Model;

class CurriculumPlanDetail extends Model
{
    protected $table = 'curriculum_plan_details';
    protected $fillable = [
        'id', 'curriculum_plan_id', 'course_id', 'is_mandatory', 'rules'
    ];


    public function curriculum_plan(){
        return $this->belongsTo(CurriculumPlan::class);
    }

    public function course(){
        return $this->belongsTo(Course::class);
    }

}
