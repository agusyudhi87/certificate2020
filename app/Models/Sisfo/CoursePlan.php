<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\Slot;
use App\Models\Sisfo\Student;
use App\Models\Sisfo\CoursePlanItem;
use App\Models\Sisfo\StudentCoursePlan;
use App\Models\Sisfo\Program;

class CoursePlan extends Model
{
    protected $table = 'course_plans';
    protected $fillable = ['year','semester','status','program_id'];
    /**
     * CoursePlan has many Slot.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function slot()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = coursePlan_id, localKey = id)
    	return $this->hasMany(Slot::class,'course_plan_id');
    }

    /**
     * CoursePlan has many Student.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function student()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = coursePlan_id, localKey = id)
    	return $this->hasMany(Student::class,'course_plan_id');
    }

    /**
     * CoursePlan has many Courseplanitem.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseplanitem()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = coursePlan_id, localKey = id)
    	return $this->hasMany(Courseplanitem::class,'course_plan_id')->orderBy('course_id');
    }

    /**
     * CoursePlan has many Studentcourseplan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function studentcourseplan()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = coursePlan_id, localKey = id)
        return $this->hasMany(StudentCoursePlan::class,'course_plan_id');
    }

    /**
     * CoursePlan belongs to Program.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function program()
    {
    	// belongsTo(RelatedModel, foreignKey = program_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Program::class,'program_id');
    }

}
