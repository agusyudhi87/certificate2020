<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Employee;
use App\Models\Sisfo\Position;
use Illuminate\Database\Eloquent\Model;

class EmployeePosition extends Model
{

    protected $fillable = [
        'id', 'employee_id', 'position_id', 'effective_date', 'expired_date'
    ];

   public function employee(){
       return $this->belongsTo(Employee::class);
   }

   public function position(){
       return $this->belongsTo(Position::class);
   }

}
