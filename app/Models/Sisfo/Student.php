<?php

namespace App\Models\Sisfo;

use App\Models\Auth\User;
use App\Models\Sisfo\Program;
use App\Models\Sisfo\StudentParent;
use App\Models\Sisfo\CoursePlanItem;
use App\Models\Sisfo\StudentAddress;
use App\Models\Sisfo\GuardianLecture;
use App\Models\Sisfo\StudentAcademic;
use App\Models\Sisfo\StudentPersonal;
use App\Models\Sisfo\StudentCoursePlan;
use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\FinalProjectLecturer;

class Student extends Model
{
    protected $primaryKey = 'code'; // or null
    protected $keyType = 'string';
    public $incrementing = false;

    protected $table = 'students';
    protected $fillable = [
        'user_id', 'code', 'name', 'gender', 'program_id', 'status'
    ];

    /**
     * Student has many Studentcourseplan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function studentcourseplan()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = student_id, localKey = id)
        return $this->hasMany(StudentCoursePlan::class, 'student_id', 'code');
    }

    /**
     * Student belongs to Program.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function program()
    {
        // belongsTo(RelatedModel, foreignKey = program_id, keyOnRelatedModel = id)
        return $this->belongsTo(Program::class, 'program_id');
    }


    public function courseplanitem()
    {
        return $this->belongsToMany(CoursePlanItem::class, 'course_attendance_details', 'student_id', 'course_plan_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function final_project_lecturers()
    {
        return $this->hasMany(FinalProjectLecturer::class);
    }


    /**
     *  Students Relations -------------------------------------------
     */

    //TODO: Dode - Ubah Relasi jadi hasOne
    public function student_academics()
    {
        return $this->hasOne(StudentAcademic::class,"student_id",'code');
    }

    public function student_addresses()
    {
        return $this->hasMany(StudentAddress::class);
    }
    public function student_parents()
    {
        return $this->hasMany(StudentParent::class);
    }

    public function student_personal()
    {
        return $this->hasOne(StudentPersonal::class, "student_id", "code");
    }

    public function student_final_projects()
    {
        return $this->hasOne(StudentFinalProjects::class);
    }

    public function guardian_lectures()
    {
        return $this->hasMany(GuardianLecture::class);
    }
}
