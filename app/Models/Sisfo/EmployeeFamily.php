<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Regency;
use App\Models\Sisfo\Employee;
use Illuminate\Database\Eloquent\Model;

class EmployeeFamily extends Model
{
    protected $table = 'employee_family';
    protected $fillable = [
        'id', 'employee_id', 'name', 'type', 'regency_id', 'birthplace', 'birthdate', 'phone', 'profession', 'address'
    ];

   public function employee(){
       return $this->belongsTo(Employee::class);
   }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }
}
