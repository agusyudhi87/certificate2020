<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\EmployeePosition;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $fillable = [
        'id', 'name', 'status', 'supervisor'
    ];

    public function employee_positions()
    {
        return $this->hasMany(EmployeePosition::class);
    }

    public function supervisor_position()
    {
        return $this->belongsTo(Position::class, 'supervisor', 'id');
    }
}
