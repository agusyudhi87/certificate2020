<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Regency;
use App\Models\Sisfo\Student;
use Illuminate\Database\Eloquent\Model;

class StudentAddress extends Model
{
    protected $table = 'student_address';
    protected $fillable = [
        'id', 'student_id', 'address', 'type', 'regency_id'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }
}
