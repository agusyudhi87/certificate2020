<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;

class StudentBillingConfigs extends Model
{
    protected $primaryKey = null;
    public $incrementing = false;

    protected $table = 'student_billing_configs';
    protected $fillable = [
        'student_id', 'is_package', 'package_tuition', 'semester_tuition', 'credit_tuition'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'code');
    }
}
