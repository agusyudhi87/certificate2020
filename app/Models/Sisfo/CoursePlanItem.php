<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Slot;
use App\Models\Sisfo\Course;
use App\Models\Sisfo\Employee;
use App\Models\Sisfo\CoursePlan;
use App\Models\Sisfo\CourseSchedule;
use App\Models\Sisfo\CourseAttendance;
use App\Models\Sisfo\StudentCoursePlan;
use Illuminate\Database\Eloquent\Model;


class CoursePlanItem extends Model
{
    protected $table = 'course_plan_items';
    protected $fillable = [
        'id', 'course_plan_id', 'course_id', 'lecturer_id', 'class', 'capacity', 'rules'
    ];

    /**
     * CoursePlanItem belongs to Courseplan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseplan()
    {
        // belongsTo(RelatedModel, foreignKey = courseplan_id, keyOnRelatedModel = id)
        return $this->belongsTo(CoursePlan::class, 'course_plan_id');
    }

    /**
     * CoursePlanItem has many Courseattendance.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseattendance()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = coursePlanItem_id, localKey = id)
        return $this->hasMany(CourseAttendance::class, 'course_plan_item_id');
    }

    /**
     * CoursePlanItem has many Courseplanitemschedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseplanitemschedule()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = coursePlanItem_id, localKey = id)
        return $this->hasMany(Courseplanitemschedule::class, 'course_plan_item_id');
    }

    // public function course_schedules()
    // {
    //     return $this->hasMany(CourseSchedule::class);
    // }

    /**
     * CoursePlanItem belongs to Course.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        // belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
        return $this->belongsTo(Course::class, 'course_id');
    }

    /**
     * CoursePlanItem belongs to Lecture.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lecture()
    {
        // belongsTo(RelatedModel, foreignKey = lecture_id, keyOnRelatedModel = id)
        return $this->belongsTo(Employee::class, 'lecturer_id', 'code');
    }



    public function slot()
    {
        return $this->belongsToMany(Slot::class, 'course_schedules',  'course_plan_item_id', 'slot_id' );
    }


    public function studentcourseplan()
    {
        return $this->belongsToMany(StudentCoursePlan::class, 'student_course_plan_items','course_plan_item_id','student_course_plan_id')->withPivot('grade', 'presence')->withTimestamps();
    }
}
