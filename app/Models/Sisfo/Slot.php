<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Room;
use App\Models\Sisfo\CoursePlan;
use App\Models\Sisfo\CoursePlanItem;
use App\Models\Sisfo\CourseSchedule;
use App\Models\Sisfo\CourseAttendance;
use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{
    protected $table = 'slots';
    protected $fillable = ['course_plan_id', 'room_id', 'day_index', 'start_time', 'end_time', 'status', 'note'];




    /**
     * Slot belongs to Courseplan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseplan()
    {
        // belongsTo(RelatedModel, foreignKey = courseplan_id, keyOnRelatedModel = id)
        return $this->belongsTo(CoursePlan::class, 'course_plan_id');
    }

    /**
     * Slot belongs to Room.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        // belongsTo(RelatedModel, foreignKey = room_id, keyOnRelatedModel = id)
        return $this->belongsTo(Room::class, 'room_id');
    }

    /**
     * Slot has many Courseattendance.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseattendance()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = slot_id, localKey = id)
        return $this->hasMany(CourseAttendance::class);
    }

    public function course_schedules()
    {
        return $this->hasMany(CourseSchedule::class);
    }
}
