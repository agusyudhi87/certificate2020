<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Slot;
use App\Models\Sisfo\CoursePlanItem;
use Illuminate\Database\Eloquent\Model;


class CourseSchedule extends Model
{
    protected $table = 'course_schedules';
    protected $fillable = [
        'id', 'course_plan_item_id', 'slot_id'
    ];

   public function course_plan_item()
   {
        return $this->belongsTo(CoursePlanItem::class);
   }

   public function slot()
   {
        return $this->belongsTo(Slot::class);
   }


}
