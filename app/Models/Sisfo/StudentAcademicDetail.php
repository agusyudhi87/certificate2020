<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Course;
use App\Models\Sisfo\StudentAcademic;
use Illuminate\Database\Eloquent\Model;

class StudentAcademicDetail extends Model
{
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    protected $table = 'student_academic_details';
    protected $fillable = [
        'academic_id', 'course_id', 'credit', 'grade'
    ];

    public function student_academics()
    {
        return $this->belongsTo(StudentAcademic::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
