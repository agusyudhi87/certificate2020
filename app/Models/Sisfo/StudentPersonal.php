<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Student;
use Illuminate\Database\Eloquent\Model;

class StudentPersonal extends Model
{
    protected $primaryKey = null;
    public $incrementing = false;

    protected $table = 'student_personals';
    protected $fillable = [
        'student_id', 'birthdate', 'birthplace', 'phone', 'email'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
