<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Student;
use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\StudentAcademicDetail;

class StudentAcademic extends Model
{
    protected $table = 'student_academics';
    protected $fillable = [
        'id', 'student_id', 'total_credit', 'gpa'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function student_academic_details()
    {
        return $this->hasMany(StudentAcademicDetail::class, 'academic_id', 'id');
    }
}
