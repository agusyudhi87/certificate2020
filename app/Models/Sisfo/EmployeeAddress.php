<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Regency;
use App\Models\Sisfo\Employee;
use Illuminate\Database\Eloquent\Model;

class EmployeeAddress extends Model
{
    protected $table = 'employee_address';
    protected $fillable = [
        'id', 'employee_id', 'address', 'type', 'regency_id'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }
}
