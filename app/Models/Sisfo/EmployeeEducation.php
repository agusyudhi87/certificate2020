<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Employee;
use Illuminate\Database\Eloquent\Model;

class EmployeeEducation extends Model
{
    protected $table = 'employee_educations';
    protected $fillable = [
        'id', 'employee_id', 'school_name', 'location', 'degree', 'title', 'start_year', 'end_year', 'gpa', 'final_project'
    ];

    public function employee()
    {
        return $this->belongTo(Employee::class);
    }
}
