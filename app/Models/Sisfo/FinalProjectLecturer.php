<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Student;
use App\Models\Sisfo\Employee;
use Illuminate\Database\Eloquent\Model;

class FinalProjectLecturer extends Model
{
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;

    protected $table = 'final_project_lecturers';
    protected $fillable = [
        'student_id', 'lecturer_id', 'type'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, "lecturer_id", "code");
    }
}
