<?php

namespace App\Models\Sisfo;

use App\Models\Auth\User;
use App\Models\Sisfo\CoursePlanItem;

use App\Models\Sisfo\EmployeePersonal;
use App\Models\Sisfo\StudentCoursePlan;
use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\FinalProjectLecturer;

class Employee extends Model
{
    protected $primaryKey = 'code'; // or null
    protected $keyType = 'string';
    public $incrementing = false;

    protected $table = 'employees';
    protected $fillable = [
        'code', 'name', 'gender', 'status', 'type', 'user_id'
    ];

    /**
     * Employee has many Studentcourseplan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function studentcourseplan()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = employee_id, localKey = id)
        return $this->hasMany(StudentCoursePlan::class, 'lecturer_id', 'code');
    }

    public function guardian_lectures()
    {
        return $this->hasMany(GuardianLecture::class);
    }

    public function lecture()
    {
        // belongsTo(RelatedModel, foreignKey = user_id, keyOnRelatedModel = id)
        return $this->belongsTo(User::class, 'user_id')->role('lecture');
    }

    public function admin()
    {
        // belongsTo(RelatedModel, foreignKey = user_id, keyOnRelatedModel = id)
        return $this->belongsTo(User::class, 'user_id')->role('admin');
    }

    public function student()
    {
        // belongsTo(RelatedModel, foreignKey = user_id, keyOnRelatedModel = id)
        return $this->belongsTo(User::class, 'user_id')->role('student');
    }

    public function course_plan_items()
    {
        return $this->hasMany(CoursePlanItem::class);
    }

    public function final_project_lecturers()
    {
        return $this->hasMany(FinalProjectLecturer::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     *  Employee relations -------------------------------------------------------------
     */

    public function employee_addresses()
    {
        return $this->hasMany(EmployeeAddress::class);
    }

    public function employee_educations()
    {
        return $this->hasMany(EmployeeEducation::class);
    }

    public function employee_positions()
    {
        return $this->hasMany(EmployeePosition::class);
    }

    public function employee_families()
    {
        return $this->hasMany(EmployeeFamily::class);
    }

    public function employee_personal()
    {
        return $this->hasOne(EmployeePersonal::class, "employee_id", "code");
    }
}
