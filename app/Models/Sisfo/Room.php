<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\Slot;
use App\Models\Sisfo\Faculty;

class Room extends Model
{
    protected $table = 'rooms';
    protected $fillable = ['code','name','status','capacity','owner'];
    /**
     * Room has many Slot.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function slot()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = room_id, localKey = id)
    	return $this->hasMany(Slot::class,'room_id');
    }

    /**
     * Room belongs to Faculty.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function faculty()
    {
    	// belongsTo(RelatedModel, foreignKey = faculty_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Faculty::class,'owner');
    }

    
}
