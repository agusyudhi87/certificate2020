<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Course;
use App\Models\Sisfo\Curriculum;
use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\CurriculumPlanDetail;

class CurriculumPlan extends Model
{
    protected $table = 'curriculum_plans';
    protected $fillable = [
        'id', 'curriculum_id', 'semester', 'total_credit'
    ];

    public function course(){
        return $this->belongsToMany(Course::class, 'curriculums_plan_details', 'curriculum_plan_id', 'course_id')->withPivot('is_mandatory');
    }

    public function curriculum(){
        return $this->belongsTo(Curriculum::class,'curriculum_id');
    }


    public function curriculum_plan_details(){
        return $this->hasMany(CurriculumPlanDetail::class);
    }


}
