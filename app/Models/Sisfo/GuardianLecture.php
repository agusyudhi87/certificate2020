<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;

class GuardianLecture extends Model
{
    protected $table = 'guardian_lectures';
    protected $fillable = [
        'id','student_id', 'lecturer_id', 'effective_date', 'expiry_date'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'lecturer_id', 'code');
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
