<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Regency;
use App\Models\Sisfo\Student;
use Illuminate\Database\Eloquent\Model;

class StudentParent extends Model
{
    protected $table = 'student_parents';
    protected $fillable = [
        'id', 'student_id', 'name', 'type', 'regency_id', 'birthplace', 'birthdate', 'phone','address'
    ];

   public function student(){
       return $this->belongsTo(Student::class);
   }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }
}
