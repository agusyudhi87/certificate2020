<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\Student;
use App\Models\Sisfo\CoursePlanItem;
use App\Models\Sisfo\CoursePlanItemSchedule;
use App\Models\Sisfo\Slot;
use App\Models\Sisfo\Employee;

class CourseAttendance extends Model
{
    protected $table = 'course_attendances';
    protected $fillable = ['course_plan_item_id','slot_id','lecturer_id','conducted_on'];
    protected  $primaryKey = 'id';

    public function absen(){
        return $this->belongsToMany(Student::class, 'course_attendance_details','attendance_id', 'student_id');
    }  

    /**
     * CourseAttendance belongs to Course.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseplanitem()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(CoursePlanItem::class,'course_plan_item_id');
    }

    /**
     * CourseAttendance belongs to Slot.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function slot()
    {
    	// belongsTo(RelatedModel, foreignKey = slot_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Slot::class,'slot_id');
    }    

    public function courseplanitemschedule()
    {
        // belongsTo(RelatedModel, foreignKey = slot_id, keyOnRelatedModel = id)
        return $this->hasMany(CoursePlanItemSchedule::class,'slot_id');
    }

    /**
     * CourseAttendance belongs to Lecture.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lecture()
    {
    	// belongsTo(RelatedModel, foreignKey = lecture_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Employee::class,'lecturer_id','code');
    }
}
