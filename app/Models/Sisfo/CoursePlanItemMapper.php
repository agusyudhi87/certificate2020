<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;


class CoursePlanItemMapper extends Model
{
    protected $table = 'course_schedule_mapper';
    public $timestamps = false;

}
