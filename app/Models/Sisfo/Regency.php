<?php

namespace App\Models\Sisfo;

use App\Models\Sisfo\Province;
use App\Models\Sisfo\EmployeeFamily;
use App\Models\Sisfo\EmployeeAddress;
use Illuminate\Database\Eloquent\Model;

class Regency extends Model
{
    protected $table = 'regencies';
    protected $fillable = [
        'id', 'province_id', 'code', 'name'
    ];

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function employee_address(){
        return $this->hasMany(EmployeeAddress::class);
    }


    public function employee_family(){
        return $this->hasMany(EmployeeFamily::class);
    }
}
