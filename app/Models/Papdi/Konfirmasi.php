<?php

namespace App\Models\Papdi;

use Illuminate\Database\Eloquent\Model;

class Konfirmasi extends Model
{
    protected $table = 'konfirmasi';
    protected $fillable = [
                            'fullname',
                            'email',
                            'phone',
                            'bukti_bayar',
                            'tgl_bayar',
                            'bank',
                            'nominal',
                            'note',
                        ];

    
}
