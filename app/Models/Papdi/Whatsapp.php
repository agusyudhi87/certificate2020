<?php

namespace App\Models\Papdi;

use Illuminate\Database\Eloquent\Model;

class Whatsapp extends Model
{
    protected $table = 'whatsapp';
    protected $fillable = [
                            'fullname',
                            'phone',
                            'message',
                            'status'
                        ];

    
}
