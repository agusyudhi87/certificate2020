<?php

namespace App\Models\Papdi;

use Illuminate\Database\Eloquent\Model;

class Peserta extends Model
{
    protected $table = 'peserta';
    protected $fillable = [
                            'fullname',
                            'email',
                            'phone',
                            'profesi',
                            'institusi',
                            'status'
                        ];

    
}
