<?php

namespace App\Models\Papdi;

use Illuminate\Database\Eloquent\Model;

class SertifikasiDetail extends Model
{
    protected $table = 'sertifikasi_detail';
    protected $fillable = [
                            'name',
                            'email',
                            'tgl_event',
                            'nama_event',
                            'status'
                        ];

    
}
