<?php

namespace App\Models\Papdi;

use Illuminate\Database\Eloquent\Model;

class Sertifikasi extends Model
{
    protected $table = 'sertifikasi';
    protected $fillable = [
                            'event',
                            'tgl',
                            'file',
                            'note',
                        ];

    
}
