<?php

if (! function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (! function_exists('gravatar')) {
    /**
     * Access the gravatar helper.
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (! function_exists('home_route')) {
    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */

    // Catatan Dode
    // home_route akan dipanggil middleware DHttp\Middleware\Authenticate
    function home_route()
    {

        if (auth()->check()) {
            if (auth()->user()->hasRole(['administrator'])) {
                 return 'admin.register';
            }

        }

        return 'frontend.auth.login';
        

    }
}
