<?php

use App\Helpers\General\QueryHelper;

if (! function_exists('InsertOrUpdate')) {
    /**
     * Access the timezone helper.
     */
    function InsertOrUpdate(array $rows,$table)
    {
        return resolve(QueryHelper::class)->InsertOrUpdate($rows,$table);
    }
}
