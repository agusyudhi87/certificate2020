<?php
namespace App\Exports;
use App\Models\Papdi\SertifikasiDetail;
​
use Maatwebsite\Excel\Concerns\FromCollection;
​
class SertifikasiExport implements FromCollection
{
    public function collection()
    {
        return SertifikasiDetail::all();
    }
}