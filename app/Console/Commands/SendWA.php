<?php

namespace App\Console\Commands;

use App\Models\DatingApp\Config;
use Illuminate\Console\Command;
use App\Repositories\BaseRepository;
use App\Models\Papdi\Peserta;
use App\Models\Papdi\Whatsapp;
use App\Events\Backend\BlastWhatsapp;
use GuzzleHttp\Client;

class SendWA extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:whatsapp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending Whatsapp Periodicaly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $jmlLimit = 70;

		$allContact = Whatsapp::whereStatus(0)
		   ->limit($jmlLimit)
		   ->get();

		
   	    foreach ($allContact as $contact) {
            $contact->status = 1;
            $contact->save();
            event(new BlastWhatsapp($contact));
	    }
        return response()->json(['message' => 'Sukses']);

    }
}
