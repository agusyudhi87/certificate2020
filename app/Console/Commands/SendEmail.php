<?php

namespace App\Console\Commands;

use App\Models\DatingApp\Config;
use Illuminate\Console\Command;
use App\Repositories\BaseRepository;
use App\Models\Papdi\SertifikasiDetail;
use App\Events\Backend\BlastEventBEU;
use App\Events\Backend\BlastSertifikat;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending Email Periodicaly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $jmlLimit = 15;

		$allSertifikat = SertifikasiDetail::whereStatus(0)
		   ->limit($jmlLimit)
		   ->get();
		
   	    foreach ($allSertifikat as $sertifikat) {
	        $sertifikat->status = 1;
	        $sertifikat->save();
	    	event(new BlastSertifikat($sertifikat));
	    }
        return response()->json(['message' => 'Sukses']);

    }
}
