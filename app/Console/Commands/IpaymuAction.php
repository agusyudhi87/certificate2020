<?php

namespace App\Console\Commands;

use App\Models\DatingApp\Config;
use Illuminate\Console\Command;
use App\Repositories\BaseRepository;
use App\Models\Papdi\Peserta;
use App\Events\Backend\BlastWhatsapp;
use GuzzleHttp\Client;
use iPaymu;

class IpaymuAction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'use:ipaymu';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Using Ipaymu';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *`
     * @return mixed
     */
    public function handle()
    {


 
        $client = new Client();
        //build transaction
        // $dataPembayaran = [   
        //         'json' => [
        //             "key"=>'8308EE43-246D-490D-BEC1-0DC8A9161C59',
        //             "amount"=>'50000',
        //             "uniqid"=>'123',
        //             "notifyUrl"=>'http://baliendocrineupdate.id/notify.php',
        //             "name"=>'Taufan Halim',
        //             "phone"=>'087822063853',
        //             "email"=>'dode.agung.asmara@gmail.com'
        //         ] 
        //     ];
        // $response = $client->request('POST', 'https://my.ipaymu.com/api/bcatransfer', $dataPembayaran);

        $data = [   
                'json' => [
                    "key"=>'8308EE43-246D-490D-BEC1-0DC8A9161C59',
                    "id"=>'2139237'
                ] 
            ];
        $client = new Client();
        //saldo
        // $response = $client->request('POST', 'https://my.ipaymu.com/api/saldo', $data);
        //transaksi
        $response = $client->request('POST', 'https://my.ipaymu.com/api/transaksi', $data);

        $res = simplexml_load_string($response->getBody()->getContents());
        $res = json_decode(json_encode($res), true);;
        dd ($res);

    }
}
