<?php

namespace App\Listeners\Frontend;

use App\Events\Frontend\PesertaRegistrasi;
use App\Events\Frontend\PesertaKonfirmasi;
use App\Mail\RegisterMail;
use App\Mail\PembayaranMail;

/**
 * Class UserEventListener.
 */
class PesertaEventListener
{
    /**
     * @param $event
     */
    public function onRegistrasi($event)
    {

        logger("PAPDI Registrasi : {$event->peserta->fullname} ({$event->peserta->email})");
        \Mail::to($event->peserta->email)
                ->cc('admin@withpapdibali.id')
                ->send(new RegisterMail($event->peserta));
    }

    /**
     * @param $event
     */
    public function onKonfirmasi($event)
    {
        logger("PAPDI Konfirmasi : {$event->konfirmasi->fullname} ({$event->konfirmasi->email})");
        \Mail::to($event->konfirmasi->email)
               ->cc('admin@withpapdibali.id')
               ->send(new PembayaranMail($event->konfirmasi));
    }



    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            PesertaRegistrasi::class,
            'App\Listeners\Frontend\PesertaEventListener@onRegistrasi'
        );

        $events->listen(
            PesertaKonfirmasi::class,
            'App\Listeners\Frontend\PesertaEventListener@onKonfirmasi'
        );


    }
}
