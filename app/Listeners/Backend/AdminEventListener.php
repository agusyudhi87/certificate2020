<?php

namespace App\Listeners\Backend;

use App\Events\Backend\AdminKonfirmasi;
use App\Events\Backend\BlastSertifikat;
use App\Events\Backend\BlastEventBEU;
use App\Events\Backend\BlastWhatsapp;
use App\Events\Backend\IpaymuAction;
use App\Mail\AdminKonfirmasiMail;
use App\Mail\BlastSertifikatMail;
use App\Mail\BlastEventMail;
use GuzzleHttp\Client;
use Steevenz\Ipaymu;


/**
 * Class UserEventListener.
 */
class AdminEventListener
{

    /**
     * @param $event
     */
    public function onKonfirmasi($event)
    {
        logger("PAPDI PAYMENT SUCCESS : {$event->peserta->fullname} ({$event->peserta->email})");
        \Mail::to($event->peserta->email)
                ->cc('admin@withpapdibali.id')
                ->send(new AdminKonfirmasiMail($event->peserta));
    }    


    /**
     * @param $event
     */
    public function onBlastSertifikat($event)
    {
        logger("Blast Sertifikat : {$event->sertifikasi->nama_event} ({$event->sertifikasi->name})");

        // dd($event);
        \Mail::to($event->sertifikasi->email)
                ->send(new BlastSertifikatMail($event->sertifikasi))
                ;
    }    


    /**
     * @param $event
     */
    public function onBlastEvent($event)
    {
        logger("Blast EVENT : {$event->sertifikasi->nama_event} ({$event->sertifikasi->name})");

        \Mail::to($event->sertifikasi->email)
                ->send(new BlastEventMail($event->sertifikasi));
    }


    /**
     * @param $event
     */
    public function onIpaymu($event){
        $ipaymu = new Ipaymu();
        $ipaymu->setApiKey('8308EE43-246D-490D-BEC1-0DC8A9161C59');
        $account = $ipaymu->getAccount();

        dd($account);
    }
    /**
     * @param $event
     */
    public function onBlastWA($event)
    {
        logger("Blast WA EVENT : ({$event->whatsapp->fullname})");

             $data = [   
                    'json' => [
                        "to"=>$event->whatsapp->phone,
                        "pesan"=>"
 Selamat Siang {$event->whatsapp->fullname},

Terima kasih untuk mengikuti webinar dari firat medic. Mohon maaf, jika terjadi kesalahan teknis terkait waktu zoom

Berikut link rekaman webinar 

https://youtu.be/Z4MKSc1S140

Materi webinar jg dapat diakses di link ini

https://drive.google.com/file/d/12y86jF8zjWtsXdSSRzrBCUf0hzHgm9Hi/view?usp=drivesdk

Saya mengundang teman2 untuk mengikuti kelas ACLS dan PALS provider course bulan Desember 2020, informasinya dapat diakses di link ini

bit.ly/ACLSDESEMBER2020

bit.ly/PALSDESEMBER2020

Sampai jumpa,
dr. Taufan Rivay Halim, M.M                                    
"
                    ] 
                ];
            $client = new Client();
            //local
            // $response = $client->request('POST', 'http://localhost:8000/waapi/sendText',  $data );
            //server
            $response = $client->request('POST', 'http://156.67.217.179:8000/waapi/sendText',  $data );

            return $response;
        
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            AdminKonfirmasi::class,
            'App\Listeners\Backend\AdminEventListener@onKonfirmasi'
        );

        $events->listen(
            BlastSertifikat::class,
            'App\Listeners\Backend\AdminEventListener@onBlastSertifikat'
        );

        $events->listen(
            BlastEventBEU::class,
            'App\Listeners\Backend\AdminEventListener@onBlastEvent'
        );

        $events->listen(
            BlastWhatsapp::class,
            'App\Listeners\Backend\AdminEventListener@onBlastWA'
        );

        $events->listen(
            IpaymuAction::class,
            'App\Listeners\Backend\AdminEventListener@onIpaymu'
        );



    }
}
