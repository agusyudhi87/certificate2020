<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Sisfo\CoursePlan;
use App\Models\Sisfo\StudentCoursePlan;
use App\Models\Sisfo\StudentAcademic;
use App\Models\Sisfo\StudentAcademicDetail;
use App\Models\Sisfo\CoursePlanItem;
use Carbon\Carbon;

//use Your Model

/**
 * Class CoursePlanRepository.
 */
class StudentCoursePlanRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(StudentCoursePlan $model)
    {
        $this->model = $model;
    } 

    public function showplanmatkul($idProgram,$semester)
    {

        return CoursePlanItem::with(['course','lecture','slot'])
                            ->has('slot')
                            ->whereHas('courseplan',function($query) use($idProgram,$semester) {
                                $query->where('program_id',$idProgram);
                                $query->where('semester',$semester);
                            })
                            ->get();
    }    

    public function searchshowplanmatkul($data,$idProgram,$semester)
    {

        return CoursePlanItem::with(['course','lecture'])
                            ->whereHas('courseplan',function($query) use($idProgram) {
                                $query->where('program_id',$idProgram);
                                $query->where('semester',$semester);
                            })
                            ->whereHas('course',function($query) use($data) {
                                $query->where('name','LIKE','%'.$data['keyword'].'%');
                            })
                            ->get();
    }

    public function get(){
          return StudentCoursePlan::
                    with('student','student.program','student.program.faculty','courseplanitem','courseplanitem.course','courseplanitem.lecture')
                    ->has('courseplanitem')
                    ->get();                 
    }       


    public function search(array $data){
          return StudentCoursePlan::
                    with('student','student.program','student.program.faculty','courseplanitem','courseplanitem.course','courseplanitem.lecture')
                    ->whereHas('student',function($query) use($data) {
                        $query->where('fullname','LIKE','%'.$data['keyword'].'%');
                    })
                    ->get();                 
    }    


    public function getStudentCourseplanItem(array $data){
        $data['courseplan'] = json_decode($data['courseplan'],true);
        // dd($data);
        return StudentCoursePlan::has('courseplanitem')
                    ->with('student','student.program','student.program.faculty','courseplanitem','courseplanitem.course','courseplanitem.lecture')
                    ->where('student_id',$data['idstudent'])
                    ->where('semester',$data['courseplan']['semester'])
                    ->first();                 
    }
    //approve lecture
    public function approveLecture(array $data)
    {
        $data['courseplanitem'] = json_decode($data['courseplanitem']);
            // dd($courseplanitem);

        return DB::transaction(function () use ($data) {    
            //change status to Student Course Plan
            $studentcourseplan = StudentCoursePlan::find($data['id']);
            $codelecture = auth()->user()->employee()->get()->first();
            $studentcourseplan->approved_by = $codelecture['code'];
            $studentcourseplan->approved_on = Carbon::now();
            $studentcourseplan->status = 1;
            $studentcourseplan->save();

            //Add Student Academics
            $dataStudentAcademics = ['student_id'=>$data['student_id']];
            $studentacademics = StudentAcademic::updateOrCreate($dataStudentAcademics);
            // dd($studentacademics->id);
            //add Student Academics Detail
            $courseplanitem = $data['courseplanitem'];
            $courseplanitem = collect($courseplanitem);
            // dd($courseplanitem);

            $dataStudentAcademicsDetail = $courseplanitem->map(function ($item,$key) use ($studentacademics) {
                                    $data ['academic_id']= $studentacademics->id;
                                    $data ['credit']= $item->course->credit;
                                    $data ['grade']= $item->pivot->grade;
                                    $data ['course_id']= $item->course->id;
                                    return $data;
                                })->toArray();



            $studentAcademicsDetail = InsertOrUpdate($dataStudentAcademicsDetail,'student_academic_details');

            //

        });
    } 

    public function approveStudent(array $data)
    {

        $data['studentcourseplan'] = json_decode($data['studentcourseplan'],true);
        return DB::transaction(function () use ($data) {    
            $studentcourseplan = StudentCoursePlan::find($data['studentcourseplan']['id']);
            //status waiting
            $studentcourseplan->status = 2;
            $studentcourseplan->save();
        });
    }

    public function delete(array $data)
    {
        return DB::transaction(function () use ($data) {
            $studentcourseplan = StudentCoursePlan::findOrFail($data['studentcourseplanid']);
            $courseplanselected =  CoursePlanItem::findOrFail($data['courseplanitemid']);
            // $courseplanselected = json_decode($data['matkul'],true);
            $studentcourseplan->courseplanitem()->detach($courseplanselected);
            $courseplanselected->capacity = $courseplanselected->capacity + 1; 
            $courseplanselected->save(); 

            // return true;
            return $studentcourseplan;                                    
            throw new GeneralException(__('exceptions.frontend.orders.update_error'));
            
        });
    }
    
    public function create(array $data)
    {

        return DB::transaction(function () use ($data) {

            $mytime = Carbon::now();
            $data['student'] = json_decode($data['student'],true);
            $data['courseplanitems'] = json_decode($data['courseplanitem'],true);
            $data['courseplan'] = json_decode($data['courseplan'],true);

            if($data['studentcourseplanid']==='undefined'){
                //Save STUDENT COURSE PLAN
                $studentcourseplan = new StudentCoursePlan;
                $studentcourseplan->course_plan_id = $data['courseplan']['id'];
                $studentcourseplan->student_id = $data['student']['code'];
                $studentcourseplan->semester = $data['courseplan']['semester'];
                $studentcourseplan->total_credit = 0;
                $studentcourseplan->gpa = 0;
                // $studentcourseplan->status = ($data['action']==='final')?2:1;
                $studentcourseplan->status = 1;
                $studentcourseplan->save();
            }else{
                $studentcourseplan = StudentCoursePlan::findOrFail($data['studentcourseplanid']); 
            }
            $dataSync = array();
            // //Save STUDENT COURSE PLAN ITEM
            foreach($data['courseplanitems'] as $key => $courseplanitem){                
                $dataSync[$courseplanitem['id']] = ['grade' => 0,'presence'=>0];  
            }
            // $studentcourseplan->courseplanitem()->sync($dataSync);
            $studentcourseplan->courseplanitem()->attach($dataSync);
          
            //Problem disini karena laravel men-SYNC data dan menghapus struktur lainya
            //update capacity
            foreach($dataSync as $key => $value){ 
                $courseplanitem = CoursePlanItem::findOrFail($key); 
                $courseplanitem->capacity = $courseplanitem->capacity - 1; 
                $courseplanitem->save(); 
            }

            // return true;
            return $studentcourseplan;                                    
            throw new GeneralException(__('exceptions.frontend.orders.update_error'));
        });
    }


}
