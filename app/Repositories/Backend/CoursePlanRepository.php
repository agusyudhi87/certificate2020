<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Sisfo\CoursePlan;
use Carbon\Carbon;

//use Your Model

/**
 * Class CoursePlanRepository.
 */
class CoursePlanRepository extends BaseRepository
{
   
 /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(CoursePlan $model)
    {
        $this->model = $model;
    } 

    public function create(array $data){
        $insertData=[
            "year" => $data['year'],
            "semester" => $data['semester'],
            "program_id" => $data['program']->id,
            "status" => $data['status']
        ];
        return DB::transaction(function () use ($insertData) {
            $model = $this->model::create($insertData);
            return $model;                                    
        });

    }     

    public function getList(){
        return $this->model::with('program.faculty')->orderBy('created_at', 'desc')->get();

          // throw new CustomException("My Custom Message");

    }    

    public function update($data, String $id){
        $insertData=array(
            "year" => $data['year'],
            "semester" => $data['semester'],
            "program_id" => $data['program']->id,
            "status" => $data['status']
        );
        return DB::transaction(function () use ($insertData,$id) {
            $model = $this->model::updateOrCreate(['id' => $id],$insertData);
            
            return $model;                                    
        });
    }

    public function delete(String $id){
        $model = $this->model::find($id);
        $model->delete();
    }



}
