<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Sisfo\CourseAttendance;

//use Your Model

/**
 * Class CourseAttendanceRepository.
 */
class CourseattendanceRepository extends BaseRepository
{
   /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(CourseAttendance $model)
    {
        $this->model = $model;
    } 

    public function create(array $data){
        // dd($data);
         $insertData=[
            "course_plan_item_id" => $data['courseplanitem']->id,
            "slot_id" => $data['slot']->id,
            "lecturer_id" => $data['lecture']->code,
            "conducted_on" => now()
        ];
        return DB::transaction(function () use ($insertData) {
            $model = $this->model::create($insertData);
            return $model;                                    
            throw new GeneralException(__('exceptions.frontend.orders.update_error'));
        });

    }     

    public function search(array $data){
        $query = $this->model::with(['courseplanitem.course','lecture','courseplanitem.courseplan.studentcourseplan.student','courseplanitem.courseplan.program.faculty','slot.courseplan.program.faculty','slot.room'])
            ->whereHas('courseplanitem.course',function($query) use($data) {
                $query->where('name','LIKE','%'.$data['keyword'].'%');
            });

        $query = $query->orderBy('conducted_on', 'desc')->get();

        return $query;
    }     

    public function get(String $day_index  = null, String $date = null){
        $query = $this->model::with(['courseplanitem.course','lecture','courseplanitem.courseplan.studentcourseplan.student','courseplanitem.courseplan.program.faculty','slot.courseplan.program.faculty','slot.room']);


        if (!is_null($day_index)){
            $query = $query->whereHas('slot', function($item) use ($day_index){
                      $item->where('day_index',$day_index);
            }); 

            if (!is_null($date)) {
                $query = $query->whereDate('conducted_on',$date);
            }   
        }  
        
        

        $query = $query->orderBy('conducted_on', 'desc')->get();

        return $query;
    }    

    
   

    public function getAbsensi(String $idAbsensi){
        $courseAttendance = $this->model::with('absen')->has('absen')->where('id',$idAbsensi)->get();
        return $courseAttendance->first();
    }

    public function searchAbsensi(array $data, String $idAbsensi){
        $courseAttendance = $this->model::with('absen')
                                    // ->has('absen')
                                    ->where('id',$idAbsensi)
                                    ->get();
        return $courseAttendance->first();
    }


    public function setAbsensi(array $data){
        $courseAttendance = $this->model::find($data['idcourseAttendance']);
        $dataStudent = collect(json_decode($data['studentAbsensi'],true));
        $idStudents = $dataStudent->map(function($item){
            return $item['student']['code'];
        })->toArray();
        return DB::transaction(function () use ($idStudents,$courseAttendance) {
            $model = $courseAttendance->absen()->sync($idStudents);
            // dd($model);
            return $model;                                     
            throw new GeneralException(__('exceptions.frontend.orders.update_error'));
        });
    }

    public function update(array $data,string $id){
        $insertData=[
            "course_plan_item_id" => $data['courseplanitem']->id,
            "slot_id" => $data['slot']->id,
            "lecturer_id" => $data['lecture']->code,
            "conducted_on" => now()
        ];
        return DB::transaction(function () use ($insertData,$id) {
            $model = $this->model::where('id', $id)->updateOrCreate($insertData);

            return $model;                                    
            throw new GeneralException(__('exceptions.frontend.orders.update_error'));
        });
    }

    public function delete(String $id){
        $model = $this->model::find($id);
        $model->delete();
    }
}
