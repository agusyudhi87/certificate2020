<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Sisfo\Course;

//use Your Model

/**
 * Class CourseRepository.
 */
class CourseRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(Course $model)
    {
        $this->model = $model;
    } 

    public function create(array $data){

        return DB::transaction(function () use ($data) {
            $model = $this->model::create($data);
            return $model;                                    
            throw new GeneralException(__('exceptions.frontend.orders.update_error'));
        });

    }     

    public function get(){
        return $this->model::orderBy('created_at', 'desc')->get();
    }    

    public function update(array $data,string $id){
        return DB::transaction(function () use ($data,$id) {
            $model = $this->model::updateOrCreate(['id' => $id],$data);

            return $model;                                    
            throw new GeneralException(__('exceptions.frontend.orders.update_error'));
        });
    }

    public function delete(String $id){
        $model = $this->model::find($id);
        $model->delete();
    }
}
