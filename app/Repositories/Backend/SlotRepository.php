<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Sisfo\Slot;
use App\Models\Sisfo\Courseplan;

//use Your Model

/**
 * Class SlotRepository.
 */
class SlotRepository extends BaseRepository
{
   /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(Slot $model)
    {
        $this->model = $model;
    } 

    public function create(array $data){
         $insertData=[
                "course_plan_id" => $data['courseplan']->id,
                "room_id" => $data['room']->id,
                "day_index" => $data['day_index']->id,
                "start_time" => $data['start_time'],
                "end_time" => $data['end_time'],
                "status" => $data['status'],
                "note" => 'goood'
            ];

        return DB::transaction(function () use ($insertData) {

            $model = $this->model::create($insertData);
            return $model;                                    
            throw new GeneralException(__('exceptions.backend.slot.create_error'));
        });

    }     

    public function get(){
        return $this->model::with(['courseplan.program.faculty','room'])->orderBy('created_at', 'desc')->get();
    }     


    public function update(array $data,string $id){
        $insertData=[
                "course_plan_id" => $data['courseplan']->id,
                "room_id" => $data['room']->id,
                "day_index" => $data['day_index']->id,
                "start_time" => $data['start_time'],
                "end_time" => $data['end_time'],
                "status" => $data['status'],
                "note" => 'goood'
            ];
        return DB::transaction(function () use ($insertData,$id) {
            $model = $this->model::updateOrCreate(['id' => $id],$insertData);
            return $model;                                    
            throw new GeneralException(__('exceptions.backend.slot.update_error'));
        });
    }    


     public function generate(String $day_index, String $date){
        $slot = $this->model::where('day_index',$day_index)
                                        ->get();
        // dd($coursePlanItems->courseattendance()->get());

        $isInserted = false;
        $courseattendance =  $this->model::courseattendance()->get();                               
             dd($courseattendance); 
        if($courseattendance->count()==0){

            $courseAttendanceData = [];
            foreach ($coursePlanItems as $item) {
                foreach ($item->slot()->get() as $subItem) {
                    $newArr=[
                        'course_plan_item_id' => $item["id"],
                        'slot_id' => $subItem["id"],
                        'lecturer_id' => $item["lecturer_id"],
                        'conducted_on' => $date,
                    ];
                    array_push($courseAttendanceData, $newArr);
                }
            }

            // dd($courseAttendanceData);                             
            $isInserted = $this->model->courseattendance()->insert($courseAttendanceData);
        }
        
        dd($isInserted);
        $data=array(
            "status" => (($courseattendance->count()==0)?0:1)
        );
        return $data;


    }

    public function delete(String $id){
        $model = $this->model::find($id);
        $model->delete();
    }
}
