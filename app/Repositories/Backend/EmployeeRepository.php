<?php

namespace App\Repositories\Backend;

use App\Models\Auth\User;
use Illuminate\Support\Str;
use App\Models\Sisfo\Employee;
use Illuminate\Support\Facades\DB;
use App\Models\Sisfo\EmployeeFamily;
use App\Repositories\BaseRepository;
use App\Models\Sisfo\EmployeeAddress;
use App\Models\Sisfo\EmployeePersonal;
use App\Models\Sisfo\EmployeePosition;
use App\Models\Sisfo\EmployeeEducation;
use Carbon\Carbon;

//use Your Model

/**
 * Class EmployeeRepository.
 */
class EmployeeRepository extends BaseRepository
{
    protected $personalModel;
    protected $addressModel;
    protected $educationModel;
    protected $familyModel;
    protected $positionModel;
    protected $userModel;
    protected $model;

    public function __construct(
        Employee $model,
        EmployeePersonal $personalModel,
        EmployeeEducation $educationModel,
        EmployeePosition $positionModel,
        EmployeeAddress $addressModel,
        EmployeeFamily $familyModel
    ) {
        $this->personalModel = $personalModel;
        $this->addressModel = $addressModel;
        $this->educationModel = $educationModel;
        $this->familyModel = $familyModel;
        $this->positionModel = $positionModel;
        $this->model = $model;
    }

    public function all()
    {
        return  $this->model::with(['user'])->get();
    }

    public function getAccount($id)
    {
        return  $this->model::where('id', $id)->with(['user', 'employee_personal'])->get();
    }

    public function createOrUpdate($data, $id)
    {
        return DB::transaction(function () use ($data, $id) {

            $data = $this->model::updateOrCreate([
                'user_id' => $id,
                'code' => array_key_exists('code', $data) ? $data['code'] : null,
            ], [
                'code' => array_key_exists('code', $data) ? $data['code'] : Str::random(6),
                'name' => $data['name'],
                'user_id' => $id,
                'gender' => $data['gender']['id'],
                'status' => $data['status']['id'],
                'type' => $data['type']['id'],
            ]);
            return $data;
        });
    }

    public function createOrUpdatePersonal($data, $id)
    {
        $data['employee_id'] =  $id;
        $data['birthdate'] = Carbon::parse($data['birthdate'])->format('Y-m-d');
        return $this->personalModel::updateOrCreate([
            'employee_id' => $id
        ], $data);
    }

}
