<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Sisfo\CoursePlanItemMapper;
use App\Models\Sisfo\CoursePlanItem;
use Carbon\Carbon;

//use Your Model

/**
 * Class CourseplanItemRepository.
 */
class CourseplanItemMapperRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(CoursePlanItemMapper $model)
    {
        $this->model = $model;
    }

    //versi 1 simple
    public function getOverlap($id){
        $mapper = $this->model::whereCoursePlanItemId($id)->get('overlap_id')->first();
        return $mapper;
    }

    //versi 1 simple
    public function generate(){
        $coursePlanItem = CoursePlanItem::has('slot')->with('slot')->get();
        $saveData = [];
        foreach ($coursePlanItem as $pointer) {
            $overlapID = $this->mapper($coursePlanItem,$pointer);
            $data = array(
                'course_plan_item_id' => $pointer->id,
                'overlap_id' => $overlapID
            );
            array_push($saveData, $data);
        }
        $mapper = insertOrUpdate($saveData,'course_schedule_mapper');
        return $mapper;
    }

    public function mapper($coursePlanItem,$pointer)
    {
        $coursPlanMapper = [];
        $overlapID = [];
        foreach ($coursePlanItem as $item) {
            $slot = $item->slot->first();
            $ptslot = $pointer->slot->first();
            if(($item->id!=$pointer->id) && ($ptslot->day_index==$slot->day_index)){

                $ptstart = Carbon::createFromTimeString($ptslot->start_time);
                $ptend = Carbon::createFromTimeString($ptslot->end_time);
                $start = Carbon::createFromTimeString($slot->start_time);
                $end = Carbon::createFromTimeString($slot->end_time);

                if ($ptstart->between($start, $end) || $ptend->between($start, $end) || $start->between($ptstart, $ptend) || $end->between($ptstart, $ptend)) {

                    array_push($overlapID, $item->id);

                } 
            }
        }    

        return json_encode($overlapID);
        
    }

    // function insertOrUpdate(array $rows,$table){
    //     // $table = \DB::getTablePrefix().with(new self)->getTable();


    //     $first = reset($rows);

    //     $columns = implode( ',',
    //         array_map( function( $value ) { return "$value"; } , array_keys($first) )
    //     );

    //     $values = implode( ',', array_map( function( $row ) {
    //             return '('.implode( ',',
    //                 array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
    //             ).')';
    //         } , $rows )
    //     );

    //     $updates = implode( ',',
    //         array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
    //     );

    //     $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";

    //     return \DB::statement( $sql );
    // }


   
}
