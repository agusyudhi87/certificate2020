<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Sisfo\CoursePlanItem;
use App\Models\Sisfo\CourseAttendance;

//use Your Model

/**
 * Class CourseplanItemRepository.
 */
class CourseplanItemRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(CoursePlanItem $model)
    {
        $this->model = $model;
    }

    public function create(array $data){

        return DB::transaction(function () use ($data) {
            $model = $this->model::create($data);
            return $model;
            throw new GeneralException(__('exceptions.frontend.orders.update_error'));
        });

    }

    public function get(){
        return $this->model::with(['courseplan.program','course','lecture'])->orderBy('created_at', 'desc')->get();
    }


    public function generate(String $day_index, String $date){

        $status = 0; //untuk status kosong & tidak ada record
        $coursePlanItems = $this->model::with('slot','courseattendance')
                                        ->whereHas('slot',function($query) use ($day_index){
                                            $query->where('day_index',$day_index);})
                                        ->get();


        $courseattendance =  CourseAttendance::with([
                                    'courseplanitem.courseplan.studentcourseplan.student',
                                    'courseplanitem.courseplan.program.faculty',
                                    'courseplanitem.course','lecture',
                                    'courseplanitem.courseplanitemschedule.slot',
                                    'slot.room'])
                                    ->whereHas('slot',function($query) use ($day_index){
                                                        $query->where('day_index',$day_index);
                                                    })
                                    ->whereDate('conducted_on',$date)
                                    ->get();
        // dd($courseattendance);
        $idInserted=[];
        if($courseattendance->count()==0){


            foreach ($coursePlanItems as $item) {
                foreach ($item->slot()->get() as $subItem) {
                    $newArr=[
                        'course_plan_item_id' => $item["id"],
                        'slot_id' => $subItem["id"],
                        'lecturer_id' => $item["lecturer_id"],
                        'conducted_on' => $date,
                    ];
                    $idInserted[] = $this->model->courseattendance()->insertGetId($newArr);
                    $status =1; //untuk status kosong & record baru saja insert
                }
            }

            $courseattendance =  CourseAttendance::with(['slot',
                            'courseplanitem.courseplan.studentcourseplan.student',
                            'courseplanitem.courseplan.program.faculty',
                            'courseplanitem.course','lecture',
                            'courseplanitem.courseplanitemschedule.slot',
                            'slot'
                            ])
                        ->whereIn('id', $idInserted)
                        ->get();
        }


        $data = [
            'status'=>$status,
            'data'=> $courseattendance
        ];
        return $data;
    }

    public function update(array $data,string $id){
        return DB::transaction(function () use ($data,$id) {
            $model = $this->model::updateOrCreate(['id' => $id],$data);

            return $model;
            throw new GeneralException(__('exceptions.frontend.orders.update_error'));
        });
    }

    public function delete( $id){
        $model = $this->model::find($id);
        $model->delete();
    }
}
