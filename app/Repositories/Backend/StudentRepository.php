<?php

namespace App\Repositories\Backend;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\Sisfo\Student;
use App\Models\Sisfo\StudentAcademic;
use App\Models\Sisfo\StudentAcademicDetail;
use App\Models\Sisfo\StudentPersonal;
use Illuminate\Support\Facades\DB;
use App\Repositories\BaseRepository;
//use Your Model

/**
 * Class StudentRepository.
 */
class StudentRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    protected $model;
    protected $personal;
    protected $academic;
    protected $academic_det;
    public function __construct(Student $model, StudentPersonal $personal, StudentAcademicDetail $academic_det, StudentAcademic $academic)
    {
        $this->model = $model;
        $this->personal = $personal;
        $this->academic_det = $academic_det;
        $this->academic = $academic;
    }


    public function createOrUpdate($data, $id)
    {
        return DB::transaction(function () use ($data, $id) {
            $data = $this->model::updateOrCreate([
                'user_id' => $id,
                'code' => array_key_exists('code', $data) ? $data['code'] : null,
            ], [
                'code' => array_key_exists('code', $data) ? $data['code'] : Str::random(6),
                'name' => $data['name'],
                'user_id' => $id,
                'gender' => $data['gender']['id'],
                'status' => $data['status']['id'],
                'program_id' => $data['program']['id'],
            ]);
            return $data;
        });
    }

    public function createOrUpdatePersonal($data, $id)
    {
        // dd($data);
        $data['student_id'] =  $id;
        $data['birthdate'] = Carbon::parse($data['birthdate'])->format('Y-m-d');
        return $this->personal::updateOrCreate([
            'student_id' => $id
        ], $data);
    }

    public function createOrUpdateAcademic($data)
    {
        return $this->academic::updateOrCreate([
            'student_id' => $data['student_id'],
            'id' => $data['id']
        ], $data);
    }

    public function updateAcademicDetList($data, $id)
    {
        // dd($data);
        foreach ($data as $key => $value) {
            $this->academic_det::where('course_id', $value['course_id'])
                ->where('academic_id', $value['academic_id'])
                ->update([
                    'academic_id' => $value['academic_id'],
                    'course_id' => $value['course_id'],
                    'credit' => $value['credit'],
                    'grade' => $value['grade'],
                ]);
        }
    }
}
