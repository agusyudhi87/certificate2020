const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('public')
    .setResourceRoot('../') // Turns assets paths in css relative to css file
    .js('resources/js/frontend/registrasi/app.js', 'js/registrasi.js')
    .js('resources/js/frontend/konfirmasi/app.js', 'js/konfirmasi.js')
    .js('resources/js/backend/registrasi/app.js', 'js/backend-registrasi.js')
    .js('resources/js/backend/konfirmasi/app.js', 'js/backend-konfirmasi.js')
    .js('resources/js/backend/sertifikasi/app.js', 'js/sertifikasi.js')
    .extract([
        'axios',
        'lodash'
    ])
    .sourceMaps();

if (mix.inProduction()) {
    mix.version()
        .options({
            // Optimize JS minification process
            terser: {
                cache: true,
                parallel: true,
                sourceMap: true
            }
        });
} else {
    // Uses inline source-maps on development
    mix.webpackConfig({
        devtool: 'inline-source-map'
    });
}
