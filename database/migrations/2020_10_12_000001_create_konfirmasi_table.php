<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateUsersTable.
 */
class CreateKonfirmasiTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('konfirmasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('fullname');
            $table->string('email')->nullable();
            $table->string('phone');
            $table->date('tgl_bayar');
            $table->string('bank');
            $table->integer('nominal');
            $table->text('bukti_bayar')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('konfirmasi');
    }
}
