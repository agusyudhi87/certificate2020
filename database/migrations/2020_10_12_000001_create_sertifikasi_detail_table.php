<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateUsersTable.
 */
class CreateSertifikasiDetailTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('sertifikasi_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');
            $table->string('email')->nullable();
            $table->integer('status')->default(0);
            $table->string('file')->nullable();
            $table->unsignedBigInteger('sertifikasi_id');
            $table->timestamps();
        });
        Schema::table('sertifikasi_detail', function (Blueprint $table) {
            $table->foreign('sertifikasi_id')->references('id')->on('sertifikasi');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('sertifikasi_detail');
    }
}
