<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use App\Models\Auth\User;
use App\Models\Sisfo\Student;
use App\Models\Sisfo\Employee;
use App\Models\Sisfo\Program;
use App\Models\Sisfo\Faculty;
use App\Models\Sisfo\Curriculum;
use App\Models\Sisfo\CurriculumPlan;
// use App\Models\Sisfo\MapperCoursePlanItemTableSeeder;

class DatabaseSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Seed the application's database.
     */
    public function run()
    {
        Model::unguard();

        $this->truncateMultiple([
            'cache',
            'failed_jobs',
            'ledgers',
            'jobs',
            'sessions',
        ]);

        $this->call(AuthTableSeeder::class);

        Model::reguard();
    }
}
