<?php

use App\Models\Sisfo\Program;
use App\Models\Sisfo\Faculty;
use Faker\Generator;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$fakulties = Faculty::All()->pluck('id')->toArray();
 
$factory->define(Faculty::class, function (Generator $faker) {
    $namaprogram = $faker->unique()->randomElement([
            'Strata 2 (S2)', 
            'Strata 1 (S1)', 
            'Diploma 3 (D3)'
        ]);

    return [
        'code' => Str::random(6),
        'level' => '1',
        'name' => $namaprogram,
        'status' => '1',        
        'faculty_id' =>  $faker->randomElement($fakulties),        
    ];
});
