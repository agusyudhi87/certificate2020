<?php

use App\Models\Sisfo\Faculty;
use Faker\Generator;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

 
$factory->define(Faculty::class, function (Generator $faker) {
    $namafakultas = $faker->unique()->randomElement([
            'Manajemen Telekomunikasi & Media', 
            'Administrasi Bisnis & Keuangan',
            'Komunikasi Multimedia'
        ]);
    
    return [
        'code' => Str::random(6),
        'name' => $namafakultas,
        'status' => '1',        
    ];
});
